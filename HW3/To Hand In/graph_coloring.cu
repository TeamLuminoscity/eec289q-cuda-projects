/*
  graph_coloring.cu
  Tim Ambrose and Robbie Sadre
  22 February 2018
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <set>
#include <time.h>
#include <algorithm>
#include <bitset>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>

#define PRINT_RESULTS true
//the fewer number of vertices per thread, the less correct phase 1 will be
//because if each thread handles only one vertex, all vertices will initially
//be given the same color
#define PAR_VERTEX_FACTOR 2000
//change to 25 once phase 3 and 4 are done

using std::istringstream;
using std::ifstream;
using std::string;
using std::vector;
using std::bitset;
using std::cout;
using std::set;

#define PAR_GREEDY_FACTOR 13
#define BYTE 8
#define MAX_COLORS 5000
#define COLOR_THREADS 3
#define MAX_THREADS 1024
#define SHARED_MEM 49152


//---------------------Helper Functions-----------------------------------
void CheckColoring(uint8_t* graph, int V, uint16_t* color)
{
  int rowBytes = (V + BYTE - 1) / BYTE;
  bool good = true;
  int conflicts = 0;
  for (int row = 0; row < V; ++row) {
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[row * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j)) {
          if (row != i * BYTE + j && color[row] == color[i * BYTE + j]) {
          //  printf("Conflict: %-4d %-4d Color: %d\n", row, i * BYTE + j,
          //         color[row]);
            good = false;
            ++conflicts;
          }
          if (color[row] < 1) {
            printf("Vertex %d has invalid color: %d\n", row, color[row]);
            good = false;
            ++conflicts;
          }
        }
    }
  }
  if (good)
    printf("Valid Coloring!\n");
  else
    printf("Invalid! Total Conflicts: %d\n", conflicts);
}

void CreateVertexOrdering (uint16_t *vertexOrder, uint16_t V) {
  int index[] = {(int)(V * 0.1), (int)(V * 0.2), (int)(V * 0.3),
                 (int)(V * 0.4), (int)(V * 0.5), (int)(V * 0.5),
                 (int)(V * 0.6), (int)(V * 0.7), (int)(V * 0.8),
                 (int)(V * 0.9), V-1, V-1};
  for (int i = 0; i < V; ++i) {
    vertexOrder[i] = i;
    vertexOrder[V + i] = index[0];
    index[0] = index[0] - 1 < 0 ? V-1 : index[0] - 1;
    vertexOrder[2*V + i] = index[1];
    index[1] = (index[1] + 1) % V;
    vertexOrder[3*V + i] = index[2];
    index[2] = index[2] - 1 < 0 ? V-1 : index[2] - 1;;
    vertexOrder[4*V + i] = index[3];
    index[3] = (index[3] + 1) % V;
    vertexOrder[5*V + i] = index[4];
    index[4] = index[4] - 1 < 0 ? V-1 : index[4] - 1;;
    int index5 = i % 2 ? (index[5] + i/2 + 1) % V : index[5] - i/2;
    vertexOrder[6*V + i] = index5 < 0 ? V + index5 : index5;
    vertexOrder[7*V + i] = index[6];
    index[6] = (index[6] + 1) % V;
    vertexOrder[8*V + i] = index[7];
    index[7] = index[7] - 1 < 0 ? V-1 : index[7] - 1;;
    vertexOrder[9*V + i] = index[8];
    index[8] = (index[8] + 1) % V;
    vertexOrder[10*V + i] = index[9];
    index[9] = index[9] - 1 < 0 ? V-1 : index[9] - 1;;
    vertexOrder[11*V + i] = i % 2 ? i/2 : V - i/2 - 1;
    vertexOrder[12*V + i] = index[11];
    --(index[11]);
  }
}

// Read DIMACS graphs
// Assumes input nodes are numbered starting from 1
int ReadColFile(const char filename[], uint8_t **graph, int* V)
{
  string line;
  ifstream infile(filename);
  if (infile.fail()) {
    fprintf(stderr, "Failed to open %s\n", filename);
    return -1;
  }

  int num_rows, num_edges;
  int rowBytes = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    string s;
    int node1, node2;
    iss >> s;
    if (s == "p") {
      iss >> s; // read string "edge"
      iss >> num_rows;
      iss >> num_edges;
      *V = num_rows;
      rowBytes = (num_rows + BYTE - 1) / BYTE;
      cudaMallocManaged(graph, rowBytes * num_rows);
      memset(*graph, 0, rowBytes * num_rows);
      continue;
    } else if (s != "e")
      continue;

    iss >> node1 >> node2;

    // Assume node numbering starts at 1
    (*graph)[(node1 - 1) * rowBytes + (node2 - 1) / BYTE] |=
            0x80 >> ((node2 - 1) % BYTE);
    (*graph)[(node2 - 1) * rowBytes + (node1 - 1) / BYTE] |=
            0x80 >> ((node1 - 1) % BYTE);
  }
  infile.close();
  return 0;
}

//--------------------------Kernels--------------------------------------------
//Phase 1 Kernel to greedily color sets of vertices in parallel
//At the time each vertex is colored, it is not in conflict with any other
//vertex in the graph, but since multiple vertices will be colored
//simultaneously, two adjacent vertices may acidently be given the same color
__global__
void GreedyColor_1(uint8_t *graph, int V, uint16_t *coloredVs,
     uint16_t *vertexOrder) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  uint16_t *order = vertexOrder + blockIdx.x * V;
  int stride = blockDim.x;
  uint16_t *solution = coloredVs + V * blockIdx.x;

  //Greeedy: Color vertices pseudo-independently
  for (int vert = threadIdx.x; vert < V; vert += stride) {
    // colors already assigned to adjacent vertices
    bool used_colors[MAX_COLORS] = {0};
    // Process all adjacent vertices and flag their colors
    // as unavailable
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[order[vert] * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        //vertex vert is connected to vertex i * BYTE + j
        if (graphByte & (0x80 >> j))
          if (solution[i * BYTE + j])
            used_colors[solution[i * BYTE + j]] = true;
    }
    // Find the first available color
    int myColor;
    for (myColor = 1; myColor < V; ++myColor)
      if (!used_colors[myColor])
         break; // color not in used set

    solution[order[vert]] = myColor;      //assign the found color
    __syncthreads();
  }
}

//Phase 2 Kernel to greedily recolor sets of vertices grouped by same color
//Drastically reduces the number of conflicting adjacent vertices
__global__
void ReverseColorClassOrder_2(uint8_t *graph, int V, uint16_t *coloredVs,
     uint16_t k) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  int index = threadIdx.x;
  int stride = blockDim.x;

  __shared__ int classSize;
  __shared__ uint16_t colorClass[SHARED_MEM / 2 - sizeof(int)];
  uint16_t *solution = coloredVs + V * blockIdx.x;
  if (index == 0) {
    classSize = 0;
    for (int i = 0; i < V; ++i)  //fill out list of vertices of color k
      if (solution[i] == k)
        colorClass[classSize++] = i;
  }
  __syncthreads();

  //Greeedy: Color vertices pseudo-independently
  for (int vert = index; vert < classSize; vert += stride) {
    // colors already assigned to adjacent vertices
    bool used_colors[MAX_COLORS] = {0};
    // Process all adjacent vertices and flag their colors
    // as unavailable
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[colorClass[vert] * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j))
          used_colors[solution[i * BYTE + j]] = true;
    }

    // Find the first available color
    int myColor;
    for (myColor = 1; myColor < V; ++myColor)
      if (!used_colors[myColor])
         break; // color not in used set

    solution[colorClass[vert]] = myColor; // Assign the found color
    __syncthreads();
  }
}

//Phase 3 Kernel to identify all remaining conflicting vertices
__global__
void DetectConflicts_3(uint8_t *graph, int V, uint16_t *coloredVs,
     bool *confTable) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  int index = threadIdx.x;
  int stride = blockDim.x;
  uint16_t *solution = coloredVs + V * blockIdx.x;
  bool *table = confTable + V * blockIdx.x;

  for (int vert = index; vert < V; vert += stride) {
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[vert * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j))                      //connected
          if (solution[i * BYTE + j] == solution[vert])   //same color
      	    if (vert < i * BYTE + j)                      //store min vertex
      	      table[vert] = 1;
      	    else
      	      table[i * BYTE + j] = 1;
    }
   // __syncthreads();
  }
}

//Phase 4 parallelized to one thread per block to fix the conflicts
__global__
void FixConflicts_4(bool *confTable, int V, uint16_t *coloredVs, uint16_t *C) {
  uint16_t *numColors = C + blockIdx.x;
  uint16_t *solution = coloredVs + V * blockIdx.x;
  bool *table = confTable + V * blockIdx.x;

  //Assign new colors to conflicting vertices
  for (int vert = 0; vert < V; ++vert) {
    if (table[vert]) {
      solution[vert] = ++(*numColors);
    }
  }
}

//--------------------------Interface------------------------------------------

//Assigns colors to a graph based on Gebremedhin-Manne "Algorithm 2"
void GraphColoringGPU(const char filename[], int** color) {
  uint8_t *graph;
  int V;

  if (ReadColFile(filename, &graph, &V) == 0) {  //file was opened and read
    clock_t t1 = clock();
    uint16_t *coloredVs;  //solution
    cudaMallocManaged(&coloredVs, V * sizeof(uint16_t) * PAR_GREEDY_FACTOR);

    uint16_t *vertexOrder;  //store vertex orderings to try
    cudaMallocManaged(&vertexOrder, PAR_GREEDY_FACTOR * sizeof(uint16_t) * V);

    //fill array with order to process vertices
    CreateVertexOrdering(vertexOrder, V);

    //---------------------PHASE 1----------------------------------------
    //One block per vertex ordering.
    //Try greedy approach on many vertex orderings
    int blocks = PAR_GREEDY_FACTOR;
    int threads = std::max(std::min(V / PAR_VERTEX_FACTOR, MAX_THREADS), 1);

    GreedyColor_1<<<blocks, threads>>>(graph, V, coloredVs, vertexOrder);
    cudaError_t cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess)
      fprintf(stderr, "kernel 1 launch failed with error \"%s\".\n",
              cudaGetErrorString(cudaerr));

    cudaFree(vertexOrder);
    uint16_t *C;  //number of colors for each vertex ordering
    cudaMallocManaged(&C, sizeof(uint16_t) * PAR_GREEDY_FACTOR);
    int maxC = 0;
    for (int i = 0; i < PAR_GREEDY_FACTOR; ++i) { //number of colors
      int clrs = *std::max_element(coloredVs + V*i, coloredVs + V*i + V);
      if (clrs > maxC)
        maxC = clrs;
      C[i] = clrs;
    }

    //---------------------PHASE 2----------------------------------------
    for (int k = maxC; k > 0; --k) {
      //how many threads to color set of vertices of color k
      threads = COLOR_THREADS;
      ReverseColorClassOrder_2<<<blocks, threads>>>(graph, V, coloredVs, k);
      cudaerr = cudaDeviceSynchronize();  //report kernel failure
      if (cudaerr != cudaSuccess)
        fprintf(stderr, "kernel 2 launch failed with error \"%s\".\n",
                cudaGetErrorString(cudaerr));
    }

    for (int i = 0; i < PAR_GREEDY_FACTOR; ++i)   //new number of colors
      C[i] = *std::max_element(coloredVs + V*i, coloredVs + V*i + V);

    //---------------------PHASE 3----------------------------------------
    threads = MAX_THREADS;
    bool *confTable; //vertices that need to be assigned a new color (true)
    cudaMallocManaged(&confTable, V * sizeof(bool) * PAR_GREEDY_FACTOR);
    memset(confTable, false, V * sizeof(bool) * PAR_GREEDY_FACTOR);

    DetectConflicts_3<<<blocks, threads>>>(graph, V, coloredVs, confTable);
    cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess)
      fprintf(stderr, "kernel 3 launch failed with error \"%s\".\n",
              cudaGetErrorString(cudaerr));

    //---------------------PHASE 4----------------------------------------
    //color vertices in confTable sequentially
    //increases the # of colors by the # of entries in the confTable
    threads = 1;   //serial operation in each block
    FixConflicts_4<<<blocks, threads>>>(confTable, V, coloredVs, C);
    cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess)
      fprintf(stderr, "kernel 3 launch failed with error \"%s\".\n",
              cudaGetErrorString(cudaerr));

    //reassign solution to int array
    *color = new int[V];

    int minColors = MAX_COLORS * 2;
    int minColorsI = 0;
    for (int i = 0; i < PAR_GREEDY_FACTOR; ++i)
      if (C[i] < minColors) {
        minColors = C[i];
        minColorsI = i;
      }

    //copy fewest colors solution to output array
    for (int i = 0; i < V; ++i)
      (*color)[i] = coloredVs[minColorsI * V + i];

    //DONE
    clock_t t2 = clock();
    printf("Colored in: %0.3fs\n", ((float)t2 - (float)t1) / CLOCKS_PER_SEC);

    if (PRINT_RESULTS) {
      CheckColoring( graph, V, coloredVs);
      int colorCount[PAR_GREEDY_FACTOR];
      for (int i = 0; i < PAR_GREEDY_FACTOR; ++i) {
        set<uint16_t> realColors;
        for (int j = 0; j < V; ++j)
          realColors.insert(coloredVs[V*i + j]);
        colorCount[i] = (int)realColors.size();
        printf("Colors (%d): %d %d\n", i + 1, C[i], colorCount[i]);
      }
      printf("Winner: Vertex Order %d\n", minColorsI + 1);
    }

    cudaFree(graph);
    cudaFree(confTable);
    cudaFree(coloredVs);
    cudaFree(C);
  }
}
