/*
  graph_coloring.cu
  Tim Ambrose
  22 February 2018
*/


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <set>
#include <time.h>
#include <algorithm>
#include <bitset>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <cstring>
#include <vector>

#define PRINT_GRAPH false
#define PRINT_DEBUG false
//the fewer number of vertices per thread, the less correct phase 1 will be
//because if each thread handles only one vertex, all vertices will initially
//be given the same color
#define PAR_VERTEX_FACTOR 8
//change to 25 once phase 3 and 4 are done

using std::istringstream;
using std::ifstream;
using std::string;
using std::vector;
using std::bitset;
using std::cout;
using std::set;

#define BYTE 8
#define MAX_COLORS 500
#define MAX_THREADS 1024
#define SHARED_MEM 49152

//---------------------TO DELETE UPON SUBMISSION-------------------------
void PrintGraphMatrix(uint8_t *graph, int V) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  printf("vertices: %d\n", V);
  for (int row = 0; row < V; ++row) {
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[row * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j))
          printf("1");
        else
          printf("0");
    }
    printf("\n");
  }
}

void CheckColoring(uint8_t* graph, int V, uint16_t* color)
{
  int rowBytes = (V + BYTE - 1) / BYTE;
  bool good = true;
  int conflicts = 0;
  for (int row = 0; row < V; ++row) {
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[row * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j)) {
          if (row != i * BYTE + j && color[row] == color[i * BYTE + j]) {
          //  printf("Conflict: %-4d %-4d Color: %d\n", row, i * BYTE + j,
          //         color[row]);
            good = false;
            ++conflicts;
          }
          if (color[row] < 1) {
            printf("Vertex %d has invalid color: %d\n", row, color[row]);
            good = false;
            ++conflicts;
          }
        }
    }
  }
  if (good)
    printf("It's all good!\n");
  else
    printf("Total Conflicts: %d\n", conflicts);
}
//---------------------END OF DELETE REGION-------------------------

// Read DIMACS graphs
// Assumes input nodes are numbered starting from 1
int ReadColFile(const char filename[], uint8_t **graph, int* V)
{
  string line;
  ifstream infile(filename);
  if (infile.fail()) {
    fprintf(stderr, "Failed to open %s\n", filename);
    return -1;
  }

  int num_rows, num_edges;
  int rowBytes = 0;
  while (getline(infile, line)) {
    istringstream iss(line);
    string s;
    int node1, node2;
    iss >> s;
    if (s == "p") {
      iss >> s; // read string "edge"
      iss >> num_rows;
      iss >> num_edges;
      *V = num_rows;
      rowBytes = (num_rows + BYTE - 1) / BYTE;
      cudaMallocManaged(graph, rowBytes * num_rows);
      memset(*graph, 0, rowBytes * num_rows);
      continue;
    } else if (s != "e")
      continue;

    iss >> node1 >> node2;

    // Assume node numbering starts at 1
    (*graph)[(node1 - 1) * rowBytes + (node2 - 1) / BYTE] |=
            0x80 >> ((node2 - 1) % BYTE);
    (*graph)[(node2 - 1) * rowBytes + (node1 - 1) / BYTE] |=
            0x80 >> ((node1 - 1) % BYTE);
  }
  infile.close();
  return 0;
}

//--------------------------Kernels--------------------------------------------
//Phase 1 Kernel to greedily color sets of vertices in parallel
//At the time each vertex is colored, it is not in conflict with any other
//vertex in the graph, but since multiple vertices will be colored
//simultaneously, two adjacent vertices may acidently be given the same color
__global__
void GreedyColor_1(uint8_t *graph, int V, uint16_t *coloredVs,
                   uint16_t *parallelColoredVs) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x;

  //Greeedy: Color vertices pseudo-independently
  int pStepRow = 0, rep = 2;
  for (int vert = index; vert < V; vert += stride) {
  ////////////   ALSO TRY REVERSING THE ORDER AT THE END OF THE THREAD
            //   AND COLORING AGAIN

    for (int r = 0; r < rep; ++r) {
#if PRINT_DEBUG
  //  printf("Thread: %-4d doing vertex: %d\n", index, vert);
#endif

    // colors already assigned to adjacent vertices
    bool used_colors[MAX_COLORS] = {0};
    // Process all adjacent vertices and flag their colors
    // as unavailable
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[vert * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        //vertex vert is connected to vertex i * BYTE + j
        if (graphByte & (0x80 >> j) && coloredVs[i * BYTE + j])
          used_colors[coloredVs[i * BYTE + j]] = true;
    }
    // Find the first available color
    int myColor;
    for (myColor = 1; myColor < V; ++myColor)
      if (!used_colors[myColor])
         break; // color not in used set

    coloredVs[vert] = myColor;      //assign the found color
    __syncthreads();
    }

    parallelColoredVs[pStepRow++ * stride + index] = vert;
  }
}

//Phase 2 Kernel to greedily recolor sets of vertices grouped by same color
//Drastically reduces the number of conflicting adjacent vertices
__global__
void ReverseColorClassOrder_2(uint8_t *graph, int V, uint16_t *coloredVs,
     uint16_t *parallelColoredVs, int pStepWidth, int *pStepRow,
     uint16_t *colorClass, uint16_t classSize) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x;

  //Greeedy: Color vertices pseudo-independently
  for (int vert = index; vert < classSize; vert += stride) {
#if PRINT_DEBUG
  //  printf("Thread: %-4d Class: %-3d doing vertex: %d\n", index,
  //         coloredVs[colorClass[vert]], colorClass[vert]);
#endif
    // colors already assigned to adjacent vertices
    bool used_colors[MAX_COLORS] = {0};
    // Process all adjacent vertices and flag their colors
    // as unavailable
    for (int i = 0; i < rowBytes; ++i) {
      int bitStop = (i < rowBytes - 1 || V % BYTE == 0) ? BYTE : V % BYTE;
      uint8_t graphByte = graph[colorClass[vert] * rowBytes + i];
      for (int j = 0; j < bitStop; ++j)
        if (graphByte & (0x80 >> j))
          if (coloredVs[i * BYTE + j])
            used_colors[coloredVs[i * BYTE + j]] = true;
    }

    // Find the first available color
    int myColor;
    for (myColor = 1; myColor < V; ++myColor)
      if (!used_colors[myColor])
         break; // color not in used set

    //record which vertices were colored in parallel
    parallelColoredVs[*pStepRow * pStepWidth + index + 1] = colorClass[vert];

    coloredVs[colorClass[vert]] = myColor; // Assign the found color
    __syncthreads();
    if (index == 0) {
      //how many vertices colored in this parallel step
      parallelColoredVs[*pStepRow * pStepWidth] = blockDim.x;
      ++(*pStepRow);
    }
  }
}

//Phase 3 Kernel to identify all remaining conflicting vertices
__global__
void DetectConflicts_3(uint8_t *graph, int V, uint16_t *coloredVs, bool
     *confTable, uint16_t *parallelColoredVs, int pStepWidth, int num_rows) {
  int rowBytes = (V + BYTE - 1) / BYTE;
  //which thread out of all blocks
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x;

  for (int row = index; row < num_rows; row += stride) {
  uint16_t *row_in_ref = &parallelColoredVs[row*pStepWidth];
  for (int z = 0; z<pStepWidth; z++){                 //given some row get
    uint16_t zcolor = coloredVs[row_in_ref[z]]; //color of that vert
   // printf("row: %d z: %d [z]: %d\n", row, z, row_in_ref[z]);
    for(int w = z+1; w<pStepWidth; w++){
      //czech if same color
    	if  (coloredVs[row_in_ref[w]]==zcolor){
    	  uint8_t graphByte=graph[row_in_ref[z]*rowBytes+(row_in_ref[w]/BYTE)];
        //czech if connected
        if (graphByte & (0x80 >> (row_in_ref[w]%BYTE))) {
    			if (row_in_ref[z]<row_in_ref[w]){
    			  confTable[row_in_ref[z]] = 1;
    			}
    			else{
    			  confTable[row_in_ref[w]] = 1;
    			}
    		}
    	}
    }
  }

	__syncthreads();
  }
}

//--------------------------Interface------------------------------------------

//Assigns colors to a graph based on Gebremedhin-Manne "Algorithm 2"
void GraphColoringGPU(const char filename[], int** color) {
  uint8_t *graph;
  int V;

  if (ReadColFile(filename, &graph, &V) == 0) {  //file was opened and read
    clock_t t1 = clock();
    uint16_t *coloredVs;  //solution
    cudaMallocManaged(&coloredVs, V * sizeof(uint16_t));

    //Color All Vertices
    //---------------------PHASE 1----------------------------------------
    int blocks = 1;   //only one is needed
    //only 1024 threads are needed to handle up to 25624 Vertices
    //if graph has more vertices, each thread simply does more work.
    int threads = std::max(std::min(V / PAR_VERTEX_FACTOR, MAX_THREADS), 1);
    int pStepWidth = threads;
    //table of which vertices were colored at the same parallel step
    uint16_t *parallelColoredVs;
    int height = (V + threads - 1) / threads;
    cudaMallocManaged(&parallelColoredVs, threads * sizeof(uint16_t) * height);

    GreedyColor_1<<<blocks, threads>>>(graph, V, coloredVs, parallelColoredVs);
    cudaError_t cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess)
      fprintf(stderr, "kernel 1 launch failed with error \"%s\".\n",
              cudaGetErrorString(cudaerr));
    uint16_t C = *std::max_element(coloredVs, coloredVs + V);  // # of colors
#if PRINT_DEBUG
    printf("Colors from phase 1: %d\nAssignments:", C);
    for (int i = 0; i < V; ++i)
      printf(" %u", coloredVs[i]);
    printf("\n");
    CheckColoring(graph, V, coloredVs);
    for (int row = 0; row < height; ++row) {
      printf("row: %d:", row);
      for (int col = 0; col < pStepWidth; ++col)
        printf(" %d", parallelColoredVs[row * pStepWidth + col]);
      printf("\n");
    }
#endif


    //---------------------PHASE 3----------------------------------------
    threads = std::max(std::min(height / PAR_VERTEX_FACTOR, MAX_THREADS), 1);
    blocks = 1;
    bool *confTable; //vertices that need to be assigned a new color (true)
    cudaMallocManaged(&confTable, V * sizeof(bool));
    memset(confTable, false, V * sizeof(bool));

    DetectConflicts_3<<<blocks, threads>>>(graph, V, coloredVs, confTable,
      parallelColoredVs, pStepWidth, height);
    cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess)
      fprintf(stderr, "kernel 3 launch failed with error \"%s\".\n",
              cudaGetErrorString(cudaerr));
#if false//PRINT_DEBUG
    printf("ConfTable:");
    for (int i = 0; i < V; ++i)
      printf(" %d", confTable[i]);
    printf("\n");
#endif
    cudaFree(parallelColoredVs);

    //---------------------PHASE 4----------------------------------------
    //color vertices in confTable sequentially
    //increases the # of colors by the # of entries in the confTable

    //assign a new unique color to all vertices in confTable
    for (int i = 0; i < V; i++){
    	if (confTable[i]) {
        C++;   //teehee. C++ in a C++ program? We are SO meta....
			  coloredVs[i]=C;
      }
    }

    //reassign solution to int array
    *color = new int[V];

    for (int i = 0; i < V; ++i)
      (*color)[i] = coloredVs[i];
    //DONE
    clock_t t2 = clock();
    printf("Colored in: %0.3fs\n", ((float)t2 - (float)t1) / CLOCKS_PER_SEC);
#if PRINT_GRAPH
    PrintGraphMatrix(graph, V);
#endif
#if PRINT_DEBUG
    printf("Solution: ");
    for (int i = 0; i < V; ++i)
      printf(" %d", (*color)[i]);
    printf("\n");

#endif
    CheckColoring( graph, V, coloredVs);
    std::cout<<'\n'<<"colors: "<<C<<'\n';
    cudaFree(graph);
    cudaFree(confTable);
    cudaFree(coloredVs);
  }
}

//----------------TO DELETE------------------------------
int main(int argc, char const *argv[]) {
  int *colors;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s GraphColFile\n", argv[0]);
    exit(-1);
  }

  if (string(argv[1]).find(".col") != string::npos)
    GraphColoringGPU(argv[1], &colors);
  else {
    fprintf(stderr, ".col file not found\n");
    exit(-1);
  }

  return 0;
}
