/*
 BigFloat.cpp
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Implementation of BigFloat class
*/

#include "BigFloat.h"
#include "BigFloatCommon.h"

//#region Constructors
BigFloat::BigFloat() {
  _numDigits = 0;
  _dpPosition = 0;
  _postDpZeros = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  _digits = new uint64_t[BIG_FLOAT_DEFAULT];
  _size = BIG_FLOAT_DEFAULT * sizeof(DigitBlock);
}

BigFloat::BigFloat(const BigFloat &newFloat) {
  _digits = NULL;
  *this = newFloat;
}

BigFloat::BigFloat(BigFloat &newFloat) {
  _digits = NULL;
  *this = newFloat;
}

BigFloat::BigFloat(uint32_t numDigits) {
  _numDigits = 0;
  _dpPosition = 0;
  _postDpZeros = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  //reserve enough space for the digits to be stored 2 per byte
  int64_t size = (numDigits + BLOCK_DIGITS - 1) / BLOCK_DIGITS;
  _digits = new uint64_t[size];
  _size = size * sizeof(DigitBlock);
  memset(_digits, 0, _size);
}

BigFloat::BigFloat(double newFloat, uint32_t numDigits) {
  char dblString[MAX_DBL_DIGITS * 3] = {0};
  sprintf(dblString, "%0.*f", MAX_DBL_DIGITS, newFloat);
  string str = dblString;
  _digits = NULL;
  *this = BigFloat(str, numDigits);
}

BigFloat::BigFloat(const string &value, uint32_t numDigits) {
  _numDigits = 0;
  _dpPosition = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  //reserve enough space for the digits to be stored 2 per byte
  int64_t size = (numDigits + BLOCK_DIGITS - 1) / BLOCK_DIGITS;
  _digits = NULL;
  _size = size * sizeof(DigitBlock);
  *this = value;
}

BigFloat::BigFloat(const char *value, uint32_t numDigits) {
  string str = value;
  _digits = NULL;
  *this = BigFloat(str, numDigits);
}

BigFloat::~BigFloat() {
  if (this->_digits != NULL) {
    delete[] _digits;
    _digits = NULL;
  }
}
//#endregion

//#region Getters
void BigFloat::Layout(BigFloatLayoutType layoutType) {
  _layoutType = layoutType;
}

DigitBlock BigFloat::GetBlock(uint32_t idx) const{
  return _digits[idx];
}

BigFloatLayoutType BigFloat::LayoutType() const {
  return this->_layoutType;
}

BigFloatType BigFloat::Type() const {
  return this->_type;
}

uint32_t BigFloat::Size() const{
  return this->_size;
}

uint32_t BigFloat::Digits() const{
  return this->_numDigits;
}

bool BigFloat::Sign() const {
  return this->_sign;
}
//#endregion

//#region Addition
//Addition
BigFloat BigFloat::operator+(const BigFloat& right) {
  return BigFloat(*this) += right;
}

BigFloat& BigFloat::operator+=(const BigFloat& right) {
  if (right._sign != this->_sign) { //do subtraction
    if (right._sign) {
      BigFloat neg(*this);
      neg._sign = true;
      neg -= right;
      neg._sign = !neg._sign;
      *this = neg;
    }
    else {
      BigFloat neg(right);
      neg._sign = true;
      *this -= neg;
    }

    return *this;
  }

  BigFloat const *top;
  BigFloat const *bottom;
  //number with more digits before the decimal point is "top" number
  //  123.4567
  //  - 6.78
  // ----------
  if (right._dpPosition > this->_dpPosition) {
    top = &right;
    bottom = this;
  }
  else {
    top = this;
    bottom = &right;
  }

  int32_t tAfterDP = top->_numDigits - top->_dpPosition,
          bAfterDP = bottom->_numDigits - bottom->_dpPosition,
          tBeforeDP = top->_dpPosition,
          bBeforeDP = bottom->_dpPosition,
          bIdx = bottom->_numDigits - 1,
          bStart = tBeforeDP - bBeforeDP + 2,
          bEnd = bStart + bottom->_numDigits,
          tIdx = top->_numDigits - 1,
          tStart = 2,
          tEnd = tStart + top->_numDigits,
          dp = top->_dpPosition + 2;

  //how many digits we are going to need
  uint32_t sSize = (tAfterDP > bAfterDP ? tAfterDP : bAfterDP) + 1 +
                   (tBeforeDP > bBeforeDP ? tBeforeDP : bBeforeDP);
  char *sum = (char *)calloc(sSize + 3, sizeof(char));
  sum[0] = sum[1] = '0';
  int32_t sIdx;
  uint32_t c = 0;
  DigitBlock tBlk, bBlk;
  bool tFirstLoad = true, bFirstLoad = true;
  for (sIdx = sSize + 1; sIdx > 1; --sIdx) {
    if (sIdx == dp)
      sum[sIdx] = '.';
    else {
      uint32_t S;
      //get appropriate digit from top number
      if (sIdx <= tEnd && sIdx >= tStart) {
        if (tIdx  % BLOCK_DIGITS == BLOCK_DIGITS - 1 || tFirstLoad) {
          tBlk = top->_digits[tIdx / BLOCK_DIGITS];
          tFirstLoad = false;
        }
        S = GET_DIGIT(tBlk, tIdx--);
      }
      else
        S = 0;

      //get digit form bottom number
      if (sIdx <= bEnd && sIdx >= bStart) {
        if (bIdx  % BLOCK_DIGITS == BLOCK_DIGITS - 1 || bFirstLoad) {
          bBlk = bottom->_digits[bIdx / BLOCK_DIGITS];
          bFirstLoad = false;
        }
        S += GET_DIGIT(bBlk, bIdx--);
      }
      sum[sIdx] = (S + c) % BASE + '0';
      c = (S + c) / BASE;
    }
  }

  if (!right._sign)
    sum[0] = '-';

  *this = BigFloat(sum, this->_size * DIGITS_PER_BYTE);
  free(sum);
  return *this;
}
//#endregion

//#region Subtraction
//Subtraction
BigFloat BigFloat::operator-(const BigFloat& right) {
  return BigFloat(*this) -= right;
}

BigFloat& BigFloat::operator-=(const BigFloat& right) {
  if (right._sign != this->_sign) { //do addition
    BigFloat neg(right);
    neg._sign = !neg._sign;
    return *this += neg;
  }
  BigFloat const *top;
  BigFloat const *bottom;
  //top number is bigger number       7.65
  //                                - 6.789
  //                               ---------
  if (AbsLess(right, *this, this->_size * DIGITS_PER_BYTE)) {
    top = this;
    bottom = &right;
  }
  else {
    top = &right;
    bottom = this;
  }

  int32_t tAfterDP = top->_numDigits - top->_dpPosition,
          bAfterDP = bottom->_numDigits - bottom->_dpPosition,
          tBeforeDP = top->_dpPosition,
          bBeforeDP = bottom->_dpPosition,
          bIdx = bottom->_numDigits - 1,
          bStart = tBeforeDP - bBeforeDP + 2,
          bEnd = bStart + bottom->_numDigits,
          tIdx = top->_numDigits - 1,
          tStart = 2,
          tEnd = tStart + top->_numDigits,
          dp = top->_dpPosition + 2;

  //how many digits we are going to need
  uint32_t sSize = (tAfterDP > bAfterDP ? tAfterDP : bAfterDP) + 3 +
    (tBeforeDP > bBeforeDP ? tBeforeDP : bBeforeDP);
  char *sum = (char *)calloc(sSize + 3, sizeof(char));
  sum[0] = sum[1] = '0';
  int32_t sIdx;
  uint32_t borrow = 0;
  DigitBlock tBlk, bBlk;
  bool tFirstLoad = true, bFirstLoad = true;
  for (sIdx = sSize + 1; sIdx > 1; --sIdx) {
    if (sIdx == dp)
      sum[sIdx] = '.';
    else {
      int32_t D;
      //get appropriate digit from top number
      if (sIdx <= tEnd && sIdx >= tStart) {
        if (tIdx  % BLOCK_DIGITS == BLOCK_DIGITS - 1 || tFirstLoad) {
          tBlk = top->_digits[tIdx / BLOCK_DIGITS];
          tFirstLoad = false;
        }
        D = GET_DIGIT(tBlk, tIdx--);
      }
      else
        D = 0;

      int dig1 = D, dig2 = 0;
      //get digit form bottom number
      if (sIdx <= bEnd && sIdx >= bStart) {
        if (bIdx  % BLOCK_DIGITS == BLOCK_DIGITS - 1 || bFirstLoad) {
          bBlk = bottom->_digits[bIdx / BLOCK_DIGITS];
          bFirstLoad = false;
        }
        dig2 = GET_DIGIT(bBlk, bIdx--);
        D -= dig2;
      }

      sum[sIdx] = (D - borrow + BASE) % BASE + '0';
      borrow = (dig1 < dig2 + borrow);
    }
  }

  if (!right._sign)
    sum[0] = '-';

  *this = BigFloat(sum, this->_size * DIGITS_PER_BYTE);
  free(sum);
  return *this;
}
//#endregion

//#region Multiplication
//Multiplication
BigFloat BigFloat::operator*(const BigFloat& right) {
  BigFloat answer = CopyFrom(*this, this->_numDigits + right._numDigits);
  answer *= right;
  return answer;
}

BigFloat& BigFloat::operator*=(const BigFloat& right) {
  uint32_t pSize = this->_numDigits + right._numDigits;
  if (pSize > this->_size * DIGITS_PER_BYTE) {
    std::stringstream ss;
    ss << "Product (" << pSize << ") would exceed the number of digits ("
       << this->_size * 2 << ") allocated for this BigFloat";
    throw std::runtime_error(ss.str());
  }
  BigFloat const *top;
  BigFloat const *bottom;
  // top number has more digits    12345.6789
  //                               * 987654.3
  //                              ------------
  if (right._numDigits > this->_numDigits) {
    top = &right;
    bottom = this;
  }
  else {
    bottom = &right;
    top = this;
  }

  uint32_t tAfterDP = top->_numDigits - top->_dpPosition,
           bAfterDP = bottom->_numDigits - bottom->_dpPosition;

  uint32_t iWidth = bottom->_numDigits;
  uint32_t tDigits = top->_numDigits;
  int *intermediate = (int *)calloc(pSize * iWidth, sizeof(int));

  DigitBlock tBlk, bBlk;
  uint32_t offset = 0;
  for (int32_t b = iWidth - 1; b >= 0; --b) {
    if (b % BLOCK_DIGITS == BLOCK_DIGITS - 1 ||
        b % iWidth == iWidth - 1)
      bBlk = bottom->_digits[b / BLOCK_DIGITS]; //retreive 64-bit block
    uint8_t bDig = GET_DIGIT(bBlk, b);
    uint8_t c = 0;
    for (int32_t t = tDigits - 1; t >= 0; --t) {
      if (t % BLOCK_DIGITS == BLOCK_DIGITS - 1 ||
          t % tDigits == tDigits - 1)
        tBlk = top->_digits[t / BLOCK_DIGITS]; //retreive 64-bit block
      uint8_t tDig = GET_DIGIT(tBlk, t);
      uint8_t p = bDig * tDig;
      intermediate[(tDigits - 1 - t + offset) * iWidth + (iWidth - 1 - b)] = (p + c) % BASE;
      c = (p + c) / BASE;
    }
    intermediate[(tDigits + offset) * iWidth + (iWidth - 1 - b)] = c % BASE;
    ++offset;
  }

#if PRINT_DEBUG
  printf("intermediate\n");
  for (uint32_t i = 0; i < pSize; ++i) {
    for (uint32_t j = 0; j < iWidth; ++j)
      printf("%-13d", intermediate[i * iWidth + j]);
    printf("\n");
  }
#endif

  uint32_t c = 0;
  char *product = (char *)calloc(pSize + 3, sizeof(char));
  int32_t pIdx = pSize + 1,
          dp = pSize - tAfterDP - bAfterDP + 1;  //sum # of decimal places
  for (int32_t digit = pSize - 1; digit >= 0; --digit) {
    uint32_t sum = 0;
    for (uint32_t col = 0; col < iWidth; ++col)
      sum += intermediate[(pSize - 1 - digit) * iWidth + col];
    product[pIdx--] = ((sum + c) % BASE) + '0';
    if (pIdx == dp)
      product[pIdx--] = '.';
    c = (sum + c) / BASE;
  }

  while (pIdx >= 0)
    product[pIdx--] = '0';
  if (top->_sign != bottom->_sign)
    product[0] = '-';

  free(intermediate);

 // string prod = product;
  *this = BigFloat(product, this->_size * DIGITS_PER_BYTE);
  free(product);
  return *this;
}
//#endregion

//#region Division
//Division
BigFloat BigFloat::operator/(const BigFloat& right) {
  BigFloat answer = CopyFrom(*this, this->_numDigits + right._numDigits);
  answer /= right;
  return answer;
}

BigFloat& BigFloat::operator/=(const BigFloat& right) {

  BigFloat const *top = this;
  BigFloat const *bottom = &right;
  // no choice for top number        numerator
  //                               -------------
  //                                denominator

  uint32_t tSize = top->_size * DIGITS_PER_BYTE;
  uint32_t qSize = tSize + bottom->_numDigits - bottom->_dpPosition;
  int8_t *num = (int8_t *)calloc(qSize, sizeof(uint8_t));
  char *quotient = (char *)calloc(qSize + 3, sizeof(char));

  uint32_t numerDigits = top->_numDigits;
  DigitBlock blk;
  for (int i = 0; i < numerDigits; ++i) {
    if (i % BLOCK_DIGITS == 0)
      blk = top->_digits[i / BLOCK_DIGITS];
    num[i] = GET_DIGIT(blk, i);
  }


  BigFloat den = CopyFrom(*bottom, tSize * 2);
  uint32_t bAfterDP = bottom->_numDigits - bottom->_dpPosition;
  den._dpPosition += bAfterDP;
  den._sign = true;
  std::stringstream ss;
  ss << den;
  den = ss.str();
  uint32_t winStart = 0,                      //start of window
           //shift quotient dp position
           dp = top->_dpPosition + 1 + bAfterDP,
           qIdx = 1;
  quotient[0] = '0';
  bool zero = false;
  blk = 0;

  //slide numerator comparison window until remainder zero or run out of
  //current space allocated for digits
  for (uint32_t shift = 0; shift < qSize && !zero; ++shift) {
    int32_t q = 0;
    BigFloat window = DigitConcat(num + winStart, shift - winStart + 1);
    BigFloat winRef = window;
    //how many times does denominator go into window
    for (; q <= BASE && !AbsLess(window, den, window._size * DIGITS_PER_BYTE);
         ++q)
      window -= den;

    if (q > BASE)
      printf("q > %d: %d\n", BASE, q);
    if (q) { //q goes into window at least once
      DigitBlock winBlk;

      //fill in zeros in numerator
      int32_t diff = winRef._dpPosition - window._dpPosition;
      for (int i = winStart; i < winStart + diff; ++i)
        num[i] = 0;

      //subtract from numerator
      winStart += diff;
      for (int i = winStart; i <= shift; ++i) {
        if ((i - winStart) % BLOCK_DIGITS == 0)
          winBlk = window._digits[(i - winStart) / BLOCK_DIGITS];
        num[i] = GET_DIGIT(winBlk, (i - winStart));
      }
      while (winStart < shift + 1 && num[winStart] == 0)
        ++winStart;
      quotient[qIdx++] = q + '0';  //add digit to answer
    }
    else
      quotient[qIdx++] = '0';
    if (qIdx == dp)
      quotient[qIdx++] = '.';

    //is the numerator (remainder) zero
    if (!zero) {
      zero = true;
      for (int i = winStart; i < qSize; ++i) {
        if (num[i] < 0)
          printf("num[i] < 0: %d !\n", num[i]);
        if (num[i] != 0) {
          zero = false;
          break;
        }
      }
    }
  }
  free(num);
  if (qIdx < qSize + 2)
    quotient[qIdx++] = '0';
  char *p = std::find(quotient, quotient + qSize + 3, '.');
  while (qIdx < qSize + 2 && p == quotient + qSize + 3) {
    if (qIdx == dp) {
      quotient[qIdx++] = '.';
      break;
    }
    else
      quotient[qIdx++] = '0';
  }
  if (right._sign != this->_sign)
    quotient[0] = '-';

  *this = BigFloat(quotient, this->_size * DIGITS_PER_BYTE);
  free(quotient);
  return *this;
}
//#endregion

//#region Power
//Power
BigFloat BigFloat::operator^(const BigFloat& right) {
  return BigFloat(*this) ^= right;
}

BigFloat& BigFloat::operator^=(const BigFloat& right) {
  return *this;
}
//#endregion

//#region Helpers
bool BigFloat::AbsLess(const BigFloat& left, const BigFloat& right,
                          uint32_t size) {
  std::stringstream ss1, ss2;
  ss1 << left;
  ss2 << right;
  BigFloat newLeft(ss1.str(), size);
  BigFloat newRight(ss2.str(), size);
  uint32_t rAfterDP = newRight._numDigits - newRight._dpPosition,
           lAfterDP = newLeft._numDigits - newLeft._dpPosition,
           rBeforeDP = newRight._numDigits - rAfterDP,
           lBeforeDP = newLeft._numDigits - lAfterDP;
  if (lBeforeDP < rBeforeDP)
    return true;
  if (rBeforeDP < lBeforeDP)
    return false;

  DigitBlock lBlk, rBlk;
  uint32_t end = (rAfterDP > lAfterDP ? lAfterDP : rAfterDP) + lBeforeDP;
  for (uint32_t i = 0; i < end; ++i) {
    if (i % BLOCK_DIGITS == 0) {
      lBlk = newLeft._digits[i / BLOCK_DIGITS];
      rBlk = newRight._digits[i / BLOCK_DIGITS];
    }
    uint8_t l = GET_DIGIT(lBlk, i);
    uint8_t r = GET_DIGIT(rBlk, i);
    if (l < r)
      return true;
    if (r < l)
      return false;
  }

  return newLeft._numDigits < newRight._numDigits;
}

BigFloat BigFloat::DigitConcat(int8_t *start, int32_t length) {
  string str = "";
  for (int i = 0; i < length; ++i)
    str += *(start + i) + '0';

  return BigFloat(str);
}

BigFloat CopyFrom(const BigFloat& right, uint32_t numDigits) {
  int64_t rSize = right._size * DIGITS_PER_BYTE;
  if (numDigits < rSize)
    numDigits = rSize;
  BigFloat bf(numDigits);
  bf._numDigits = right._numDigits;
  bf._dpPosition = right._dpPosition;
  bf._postDpZeros = right._postDpZeros;
  bf._sign = right._sign;
  bf._layoutType = right._layoutType;
  bf._type = right._type;
  memcpy(bf._digits, right._digits, right._size);
  return bf;
}
//#endregion

//#region Copy and Output
BigFloat& BigFloat::operator=(const BigFloat& right) {
  if (this->_digits != NULL)
    delete[] this->_digits;
  _numDigits = right._numDigits;
  _size = right._size;
  _dpPosition = right._dpPosition;
  _postDpZeros = right._postDpZeros;
  _sign = right._sign;
  _layoutType = right._layoutType;
  _type = right._type;
  _digits = new uint64_t[_size / sizeof(DigitBlock)];
  memcpy(this->_digits, right._digits, _size);
  return *this;
}

//Assign string to BigFloat
BigFloat& BigFloat::operator=(const string& newFloat) {
  if (this->_digits != NULL)
    delete[] this->_digits;
  _digits = new uint64_t[_size / sizeof(DigitBlock)];
  string cFloat = FixExtraZeros(newFloat);
  _numDigits = DigitsOf(cFloat, &_dpPosition, &_sign, &_postDpZeros);
  if (_numDigits < 1) {    //string couldn't be interpreted as a float
    std::stringstream ss;
    ss << "BigFloat string assignemnt: invalid float string: "
       << newFloat << "\n";
    throw std::invalid_argument(ss.str().c_str());
  }

  //construction of integer bits from digits
  uint64_t d = 0;
  uint64_t blk = 0;
  uint32_t fChars = cFloat.size();
  for (uint32_t i = 0; i < fChars; ++i) {
    int64_t digit = cFloat[i] - '0';    //convert character to int
    if (digit >= 0 && digit < BASE) {
      blk |= SET_DIGIT(d, digit);
      if (d % BLOCK_DIGITS == BLOCK_DIGITS - 1) { //store 64-bit block
#if PRINT_DEBUG
        printf("%0*llX   ", BLOCK_DIGITS, blk);
#endif
        if (d < _size * DIGITS_PER_BYTE)
          _digits[d / BLOCK_DIGITS] = blk;
        blk = 0;
      }
      ++d;
    }
  }
#if PRINT_DEBUG
  printf("%0*llX   \n", BLOCK_DIGITS, blk);
#endif
  if (d < _size * DIGITS_PER_BYTE)
    _digits[d / BLOCK_DIGITS] = blk;

  return *this;
}

//Print out number as stream
std::ostream & operator<<(std::ostream &out, const BigFloat &f) {
  uint32_t d = f._numDigits;
  if (!f._sign)
    out << '-';   //print negative sign if needed
  uint32_t dPos = f._dpPosition;
  uint64_t blk = 0;
  uint32_t i;

  for (i = 0; i < d; ++i) {
    if (i % BLOCK_DIGITS == 0)
      blk = f._digits[i / BLOCK_DIGITS]; //retreive 64-bit block
    if (i == dPos)  //output decimal point
      out << '.';
    out << GET_DIGIT(blk, i);   //retreive 4 bits from 64-bit block
  }
  if (i == dPos)
    out << '.';

  return out;
}
//#endregion
