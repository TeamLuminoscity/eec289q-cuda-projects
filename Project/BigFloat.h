/*
 BigFloat.h
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Library for CPU implementation of floating point arithmetic of greater than
   64-bit precision using 4 bits to store each digit
*/

#ifndef BIG_FLOAT_H
#define BIG_FLOAT_H

#include "BigFloatCommon.h"

class BigFloat
{
private:
  uint32_t _numDigits;
  uint32_t _size;
  uint32_t _dpPosition;
  uint32_t _postDpZeros;
  bool _sign;  //positive
  BigFloatLayoutType _layoutType;
  BigFloatType _type;
  DigitBlock *_digits;// [BIG_FLOAT_MAX_SPACE];
  //is the magnitude of left less than right
  bool AbsLess(const BigFloat& left, const BigFloat& right, uint32_t size);
public:
  BigFloat();
  BigFloat(const BigFloat &newFloat);
  BigFloat(BigFloat &newFloat);
  BigFloat(uint32_t numDigits);
  //throws std::invalid_argument exception
  BigFloat(double newFloat, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  //throws std::invalid_argument exception
  BigFloat(const string &value, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  //throws std::invalid_argument exception
  BigFloat(const char *value, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  ~BigFloat();

  void Layout(BigFloatLayoutType layoutType);
  DigitBlock GetBlock(uint32_t idx) const;
  BigFloatLayoutType LayoutType() const;
  BigFloatType Type() const;
  uint32_t Size() const;
  uint32_t Digits() const;
  bool Sign() const;

  //Addition
  BigFloat operator+(const BigFloat& right);
  BigFloat& operator+=(const BigFloat& right);
  //Subtraction
  BigFloat operator-(const BigFloat& right);
  BigFloat& operator-=(const BigFloat& right);
  //Multiplication
  BigFloat operator*(const BigFloat& right);
  BigFloat& operator*=(const BigFloat& right);
  //Division
  BigFloat operator/(const BigFloat& right);
  BigFloat& operator/=(const BigFloat& right);
  //Power
  BigFloat operator^(const BigFloat& right);
  BigFloat& operator^=(const BigFloat& right);

  //Assembles a BigFloat representing an integer with digits in array
  //from *start to start + length
  BigFloat DigitConcat(int8_t *start, int32_t length);


  friend BigFloat CopyFrom(const BigFloat& right, uint32_t numDigits = 0);

  BigFloat& operator=(const BigFloat& right);
  //throws std::invalid_argument exception
  BigFloat& operator=(const string& newFloat);
  //Print out number as stream
  friend std::ostream & operator<<(std::ostream &out, const BigFloat &f);
};

#endif
