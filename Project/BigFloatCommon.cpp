/*
 BigFloatCommon.cpp
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Implementation of BigFloatCommon functions
*/

#include "BigFloatCommon.h"

string FixExtraZeros(const string &str) {
//pass a str that may be a valid float representation
//gets rid of 0's in front and and end e.g. 01.10 -> 1.1
//if there is no 0 before the decimal place, insert it e.g. -.1 -> -0.1
  size_t firstDigit = str.find_first_not_of("-0");
  size_t dp = str.find_last_of('.');
  string postDP = dp == string::npos ? "" : str.substr(dp);
  size_t lastDigit = postDP.find_last_not_of('0');
  if (firstDigit == string::npos)
    return "0.0";
  string corrected = str.substr(firstDigit, dp - firstDigit);
  if (lastDigit != string::npos)
    corrected.append(postDP.substr(0, lastDigit + 1));
  size_t nsPos = str.find('-');
  if (nsPos != string::npos && str[firstDigit] == '.')
    corrected.insert(0, "-0");
  else if (nsPos != string::npos)
    corrected.insert(0, "-");
  else if (str[firstDigit] == '.')
    corrected.insert(0, "0");

  if (corrected.find('.') == string::npos)
    corrected.append(".0");

  return corrected;
}

uint32_t DigitsOf(const string &str, uint32_t *decimalPos, bool *fSign,
                  uint32_t *postDpZeros) {
// returns num digits 
// fills into two pointer values (position and sign of num)
// -15.1 -> digits = 3, decimalPos = 2 (from left), sign = False
  size_t nsPos1 = str.find_first_of('-');
  size_t nsPos2 = str.find_last_of('-');
  size_t dpPos1 = str.find_first_of('.');
  size_t dpPos2 = str.find_last_of('.');
  //Check validity of float string
  if (str.find_first_not_of("-.0123456789") == string::npos &&
      (nsPos1 == string::npos || nsPos1 == 0) && dpPos1 == dpPos2 &&
      nsPos1 == nsPos2) {
    uint32_t digits = str.size();
    if (dpPos1 != string::npos)    //don't count decimal point
      --digits;
    if (nsPos1 != string::npos) {  //don't count minus sign
      --digits;
      if (fSign != NULL)
        *fSign = false;
    }
    else if (fSign != NULL)
      *fSign = true;
    //decimal position indicates which digit the decimal point is before
    if (decimalPos != NULL)
      *decimalPos = nsPos1 != string::npos ? dpPos1 - 1 : dpPos1;
    //number of zeros just after decimal point if there are no sig figs
    //before the decimal point
    if (postDpZeros != NULL) {
      size_t postDigit = str.find_first_not_of('0', dpPos1 + 1);
      size_t preDigits = str.find_first_not_of("-0");
      if (postDigit != string::npos && preDigits == dpPos1)
        *postDpZeros = postDigit - dpPos1;
      else
        *postDpZeros = 0;
    }
    return digits;
  }
  return 0;
}
