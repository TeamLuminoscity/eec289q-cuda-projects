/*
 BigFloatCommon.h
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Definitions, Types, and Macros for BigFloat Library
*/

#ifndef BIG_FLOAT_COMMON_H
#define BIG_FLOAT_COMMON_H

#define PRINT_DEBUG 0

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdexcept>
#include <set>
#include <time.h>
#include <algorithm>
#include <bitset>
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>

using std::string;

//80 digits default (slightly more than octuple precision)
#define BIG_FLOAT_DEFAULT 5
#define BIG_FLOAT_DEF_DIGITS 80
#define BIG_FLOAT_MAX_SPACE 1000
#define DIGITS_PER_BYTE 2
#define BLOCK_SIZE 4
#define FLOAT_192_BITS 192
#define FLOAT_128_BITS 128
#define STRING_BASE 10
#define SIGN_MASK 0x8000000000000000
#define DBL_EXP_MASK 0x7FF0000000000000
#define QUAD_EXP_MASK 0x7FFF000000000000
#define QUAD_UPPER_SIG_MASK 0xFFFFFFFFFFFF
#define QUAD_EXP_SHIFT 48
#define QUAD_SIG_SHIFT 60
#define QUAD_DBL_SIG 4
#define QUAD_EXP_HEX_DIGITS 4
#define QUAD_EXP_BIAS 0x3FFF
#define QUAD_SIGN_EXP_MASK 0xFFFF
#define DBL_EXP_BIAS 0x3FF
#define DBL_INF 0x7FF
#define BLOCK_DIGITS 16
#define DIGIT_MASK 0xF
#define DIGIT_UNMASK 0xF000000000000000
#define DIGIT_BITS 4
#define BASE 10
#define BYTE 8
#define MAX_DBL_DIGITS 16
#define MAX_128_DIGITS 34
#define MAX_256_DIGITS 71
#define GLOBAL_INTERMEDIATE_FACTOR 4
#define DEFAULT_INTERMEDIATE (BIG_FLOAT_DEF_DIGITS * (BIG_FLOAT_DEF_DIGITS+1)\
                             * GLOBAL_INTERMEDIATE_FACTOR)
#define THREADS_MAX 512

typedef uint64_t DigitBlock;

typedef enum bigFloatLayoutType {
  INTUITIVE,    //Integer bit layout with int tracking the decimal position
  IEEE754       //IEEE 754 floating point style bit arrangement
} BigFloatLayoutType;

typedef enum bigFloatType {
  MultipleCPU,  //Serial   BigFloat
  MultipleGPU   //Parallel BigFloatGPU
} BigFloatType;

#define GET_DIGIT(__BLOCK__, __I__) (((__BLOCK__) >> (BLOCK_DIGITS - 1 -\
        (__I__) % BLOCK_DIGITS) * DIGIT_BITS) & DIGIT_MASK)
#define SET_DIGIT(__I__, __VAL__) ((__VAL__) <<\
        ((BLOCK_DIGITS - 1 - (__I__) % BLOCK_DIGITS) * DIGIT_BITS))

string FixExtraZeros(const string &str);

uint32_t DigitsOf(const string &str, uint32_t *decimalPos, bool *fSign,
                  uint32_t *postDpZeros);

#endif
