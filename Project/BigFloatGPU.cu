/*gFloatGPU2.cu
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Implementation of BigFloatGPU class
*/

#include "BigFloatGPU.h"
#include "BigFloatCommon.h"

//#region Kernels
__global__ void PartialSums
(int* PARTIAL_SUMS,
DigitBlock *TOP_DIGITS, DigitBlock *BOTTOM_DIGITS,
uint32_t sSize,
uint32_t tEnd, uint32_t tStart,
uint32_t bEnd, uint32_t bStart)
{
  uint32_t DIG1_TEMP, DIG2_TEMP;
  uint64_t tBlk, bBlk;
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int sIdx = index; sIdx < sSize; sIdx += stride) {
    uint32_t bIdx = sIdx - bStart;
    uint32_t tIdx = sIdx - tStart;
    DIG1_TEMP = DIG2_TEMP = 0;
    if (sIdx <= tEnd && sIdx >= tStart) {
      tBlk = TOP_DIGITS[tIdx / BLOCK_DIGITS];
      DIG1_TEMP = GET_DIGIT(tBlk, tIdx);
    }
    if (sIdx <= bEnd && sIdx >= bStart) {
      bBlk = BOTTOM_DIGITS[bIdx / BLOCK_DIGITS];
      DIG2_TEMP= GET_DIGIT(bBlk, bIdx);
    }
    PARTIAL_SUMS[sIdx] = DIG1_TEMP+DIG2_TEMP;
  }
}

__global__ void PartialDiffs
(int *PARTIAL_SUMS, uint32_t total,
DigitBlock *TOP_DIGITS, DigitBlock *BOTTOM_DIGITS,
uint32_t tEnd, uint32_t tStart,
uint32_t bEnd, uint32_t bStart,
int32_t tIdxOff, int32_t bIdxOff)
{
  uint32_t DIG1_TEMP, DIG2_TEMP;
  uint64_t tBlk, bBlk;
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  int sIdx = index + 2;
  for (; sIdx < total + 2; sIdx += stride) {
    DIG1_TEMP = DIG2_TEMP = 0;
    if (sIdx <= tEnd && sIdx >= tStart) {
      int32_t tIdx = sIdx + tIdxOff;
      tBlk = TOP_DIGITS[tIdx / BLOCK_DIGITS];
      DIG1_TEMP = GET_DIGIT(tBlk, tIdx);
    }
    if (sIdx <= bEnd && sIdx >= bStart) {
      int32_t bIdx = sIdx + bIdxOff;
      bBlk = BOTTOM_DIGITS[bIdx / BLOCK_DIGITS];
      DIG2_TEMP= GET_DIGIT(bBlk, bIdx);
    }
    PARTIAL_SUMS[sIdx] = DIG1_TEMP-DIG2_TEMP;
  }
}

__global__
void MultiplyIntermediate(DigitBlock *t_Digits, uint32_t tDigits, DigitBlock
                          *b_Digits, uint32_t iWidth, int *intermediate) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;

  DigitBlock tBlk, bBlk;
  for (int32_t b = index; b < iWidth; b += stride) {
    bBlk = b_Digits[b / BLOCK_DIGITS]; //retreive 64-bit block
    uint8_t bDig = GET_DIGIT(bBlk, b);
    uint8_t c = 0;
    for (int32_t t = tDigits - 1; t >= 0; --t) {
      if (t % BLOCK_DIGITS == BLOCK_DIGITS - 1 ||
          t % tDigits == tDigits - 1)
        tBlk = t_Digits[t / BLOCK_DIGITS]; //retreive 64-bit block
      uint8_t tDig = GET_DIGIT(tBlk, t);
      uint8_t p = bDig * tDig;
      intermediate[(tDigits - 1 - t + (iWidth - 1 - b)) * iWidth +
        (iWidth - b)] = (p + c) % BASE;
      c = (p + c) / BASE;
    }
    intermediate[(tDigits + (iWidth - 1 - b)) * iWidth + (iWidth - b)] =
      c % BASE;
  }
}

__global__
void AddIntermediateProducts(uint32_t pSize, uint32_t tDigits, uint32_t iWidth,
                             int *intermediate) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;

  for (int digit = index; digit < pSize; digit += stride) {
    uint32_t sum = 0;
    uint32_t row = digit * iWidth;
    for (uint32_t col = 1; col <= iWidth; ++col)
      sum += intermediate[row + col];
    intermediate[row] = sum;
  }
}
//#endregion

//#region Constructors
bool BigFloatGPU::intermediateInitalized = false;
int *BigFloatGPU::_intermediate = NULL;
int BigFloatGPU::intermediateSize = 0;

BigFloatGPU::BigFloatGPU() {
  _numDigits = 0;
  _dpPosition = 0;
  _postDpZeros = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  cudaMallocManaged(&_digits, BIG_FLOAT_DEFAULT * sizeof(DigitBlock));
  _size = BIG_FLOAT_DEFAULT * sizeof(DigitBlock);
  if (!intermediateInitalized) {
    intermediateSize = sizeof(int) * DEFAULT_INTERMEDIATE;
    cudaMallocManaged(&_intermediate, intermediateSize);
    intermediateInitalized = true;
#if PRINT_DEBUG
    printf("Intermediate Initialized\n");
#endif
  }
}

BigFloatGPU::BigFloatGPU(const BigFloatGPU &newFloat) {
  _digits = NULL;
  *this = newFloat;
}

BigFloatGPU::BigFloatGPU(uint32_t numDigits) {
  _numDigits = 0;
  _dpPosition = 0;
  _postDpZeros = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  //reserve enough space for the digits to be stored 2 per byte
  int64_t size = (numDigits + BLOCK_DIGITS - 1) / BLOCK_DIGITS;
  cudaMallocManaged(&_digits, size * sizeof(DigitBlock));
  _size = size * sizeof(DigitBlock);
  memset(_digits, 0, _size);
  if (!intermediateInitalized) {
    intermediateSize = sizeof(int) * size * size * BLOCK_DIGITS * BLOCK_DIGITS
                       * GLOBAL_INTERMEDIATE_FACTOR;
    cudaMallocManaged(&_intermediate, intermediateSize);
    intermediateInitalized = true;
#if PRINT_DEBUG
    printf("Intermediate Initialized\n");
#endif
  }
}

BigFloatGPU::BigFloatGPU(double newFloat, uint32_t numDigits) {
  char dblString[MAX_DBL_DIGITS * 3] = {0};
  sprintf(dblString, "%0.*f", MAX_DBL_DIGITS, newFloat);
  string str = dblString;
  _digits = NULL;
  *this = BigFloatGPU(str, numDigits);
}

BigFloatGPU::BigFloatGPU(const string &value, uint32_t numDigits) {
  _numDigits = 0;
  _dpPosition = 0;
  _sign = true;  //positive
  _layoutType = INTUITIVE;
  _type = MultipleCPU;
  //reserve enough space for the digits to be stored 2 per byte
  int64_t size = (numDigits + BLOCK_DIGITS - 1) / BLOCK_DIGITS;
  _size = size * sizeof(DigitBlock);
  cudaMallocManaged(&_digits, _size);
#if 0//PRINT_DEBUG
  printf("String Constructor\n");
#endif
  *this = value; //call operator=(string)
#if 0//PRINT_DEBUG
  printf("String assigned to BigFloat\n");
#endif
}

BigFloatGPU::BigFloatGPU(const char *value, uint32_t numDigits) {
  string str = value;
  _digits = NULL;
#if 0//PRINT_DEBUG
  printf("const char Constructor\n");
#endif
  *this = BigFloatGPU(str, numDigits); //call BigFloatGPU(string, numDigits)
#if 0//PRINT_DEBUG
  printf("String assigned to BigFloat\n");
#endif
}

BigFloatGPU::~BigFloatGPU() {
  if (this->_digits != NULL) {
    cudaFree(_digits);                     //<<<<<<<<<<<<<<<<<< DON'T LEAVE THIS COMMENTED FOREVER
    _digits = NULL;
  }
}
//#endregion

//#region Getters
void BigFloatGPU::Layout(BigFloatLayoutType layoutType) {
  _layoutType = layoutType;
}

DigitBlock BigFloatGPU::GetBlock(uint32_t idx) const{
  return _digits[idx];
}

BigFloatLayoutType BigFloatGPU::LayoutType() const {
  return this->_layoutType;
}

BigFloatType BigFloatGPU::Type() const {
  return this->_type;
}

uint32_t BigFloatGPU::Size() const{
  return this->_size;
}

uint32_t BigFloatGPU::Digits() const{
  return this->_numDigits;
}

bool BigFloatGPU::Sign() const {
  return this->_sign;
}
//#endregion

//#region Addition
//Addition
BigFloatGPU BigFloatGPU::operator+(const BigFloatGPU& right) {
  BigFloatGPU answer = CopyFrom(*this, this->_numDigits + right._numDigits);
  answer += right;
  return answer;
}

BigFloatGPU& BigFloatGPU::operator+=(const BigFloatGPU& right) {
  if (right._sign != this->_sign) { //do subtraction
    if (right._sign) {
      BigFloatGPU neg(*this);
      neg._sign = true;
      neg -= right;
      neg._sign = !neg._sign;
      *this = neg;
    }
    else {
      BigFloatGPU neg(right);
      neg._sign = true;
      *this -= neg;
    }

    return *this;
  }

  BigFloatGPU const *top;
  BigFloatGPU const *bottom;
  //number with more digits before the decimal point is "top" number
  //  123.4567
  //  - 6.78
  // ----------
  if (right._dpPosition > this->_dpPosition) {
    top = &right;
    bottom = this;
  }
  else {
    top = this;
    bottom = &right;
  }

  int32_t tAfterDP = top->_numDigits - top->_dpPosition,
          bAfterDP = bottom->_numDigits - bottom->_dpPosition,
          tBeforeDP = top->_dpPosition,
          bBeforeDP = bottom->_dpPosition,
          bStart = tBeforeDP - bBeforeDP + 2,
          bEnd = bStart + bottom->_numDigits,
          tStart = 2,
          tEnd = tStart + top->_numDigits,
          dp = top->_dpPosition + 2;

  //how many digits we are going to need
  uint32_t sSize = (tAfterDP > bAfterDP ? tAfterDP : bAfterDP) + 1 +
                   (tBeforeDP > bBeforeDP ? tBeforeDP : bBeforeDP);
  char *sum = (char *)calloc(sSize + 3, sizeof(char));

  sum[0] = sum[1] = '0';
  int32_t sIdx;

  // memset(_intermediate, 0, (sSize-1)*sizeof(int));
  memset(_intermediate, 0, intermediateSize);
  int threads = (sSize-1 > THREADS_MAX ? THREADS_MAX : sSize-1);
  int blocks = (sSize-1 + threads - 1) / threads;
  PartialSums<<<blocks,threads>>>(_intermediate, top->_digits, bottom->_digits,
   sSize-1,tEnd-2,tStart-2,bEnd-2,bStart-2);



  cudaError_t cudaerr = cudaDeviceSynchronize();  //report kernel failure
    if (cudaerr != cudaSuccess) {
      std::stringstream ss;
      ss << "Kernel VectorMultiply failed with error \""
         << cudaGetErrorString(cudaerr) << "\".";
      throw std::runtime_error(ss.str());
    }

  int CARRY=0;
//  printf("decimal point = %d",dp);
  int DIGi = sSize-2;
  int STRING_SIZE = 3 + (tAfterDP > bAfterDP ? tAfterDP : bAfterDP) +
                    (tBeforeDP > bBeforeDP ? tBeforeDP :bBeforeDP);
  char* RESULT_STR = new char[STRING_SIZE + 1];
  RESULT_STR[STRING_SIZE] = 0;

  for (sIdx = STRING_SIZE-1; sIdx>1; --sIdx){
  	if (sIdx == dp)
  		RESULT_STR[sIdx] ='.';
  	else {
  		RESULT_STR[sIdx] = '0'+(CARRY+ _intermediate[DIGi])%10;
  		CARRY = (CARRY + _intermediate[DIGi])/10;
  		DIGi--;
  	}

  }
  RESULT_STR[1]='0'+CARRY;
  RESULT_STR[0]='0';
  if (!right._sign)
      RESULT_STR[0] = '-';

  *this = BigFloatGPU(RESULT_STR, this->_size * DIGITS_PER_BYTE);
  free(sum);
  return *this;
}
//#endregion

//#region Subtraction
//Subtraction
BigFloatGPU BigFloatGPU::operator-(const BigFloatGPU& right) {
  return BigFloatGPU(*this) -= right;
}

BigFloatGPU& BigFloatGPU::operator-=(const BigFloatGPU& right) {
  if (right._sign != this->_sign) { //do addition
    BigFloatGPU neg(right);
    neg._sign = !neg._sign;
    return *this += neg;
  }
  BigFloatGPU const *top;
  BigFloatGPU const *bottom;
  //top number is bigger number       7.65
  //                                - 6.789
  //                               ---------
  if (AbsLess(right, *this, this->_size * DIGITS_PER_BYTE)) {
    top = this;
    bottom = &right;
  }
  else {
    top = &right;
    bottom = this;
  }

  int32_t tAfterDP = top->_numDigits - top->_dpPosition,
          bAfterDP = bottom->_numDigits - bottom->_dpPosition,
          tBeforeDP = top->_dpPosition,
          bBeforeDP = bottom->_dpPosition,
          bStart = tBeforeDP - bBeforeDP + 2,
          bEnd = bStart + bottom->_numDigits,
          tStart = 2,
          tEnd = tStart + top->_numDigits,
          dp = top->_dpPosition + 2;

  //how many digits we are going to need
  uint32_t sSize = (tAfterDP > bAfterDP ? tAfterDP : bAfterDP) + 3 +
    (tBeforeDP > bBeforeDP ? tBeforeDP : bBeforeDP);
  char *sum = (char *)calloc(sSize + 3, sizeof(char));
  sum[0] = sum[1] = '0';
 // memset(_intermediate, 0, (sSize+1)*sizeof(int));
  memset(_intermediate, 0, intermediateSize);
  int threads = sSize-1 > THREADS_MAX ? THREADS_MAX : sSize-1;
  int blocks = (sSize-1 + threads - 1) / threads;
  PartialDiffs<<<blocks,threads>>>(_intermediate, sSize-1, top->_digits,
    bottom->_digits, tEnd, tStart, bEnd, bStart, top->_numDigits - tEnd,
    bottom->_numDigits - bEnd);

  cudaError_t cudaerr = cudaDeviceSynchronize();  //report kernel failure
  if (cudaerr != cudaSuccess) {
    std::stringstream ss;
    ss << "Kernel PartialDiffs failed with error \""
       << cudaGetErrorString(cudaerr) << "\".";
    throw std::runtime_error(ss.str());
  }

  int32_t borrow = 0;
  int32_t sIdx;

  for (sIdx = sSize + 1; sIdx > 1; --sIdx) {
    if (sIdx == dp)
      sum[sIdx] = '.';
    else {
      int32_t D = _intermediate[sIdx];
      sum[sIdx] = (D - borrow + BASE) % BASE + '0';
      borrow = ((D-borrow)<0);
    }
  }

  if (!right._sign)
    sum[0] = '-';

  free(sum);

  return (*this = BigFloatGPU(sum, this->_size * DIGITS_PER_BYTE));
}
//#endregion

//#region Multiplication
//Multiplication
BigFloatGPU BigFloatGPU::operator*(const BigFloatGPU& right) {
  BigFloatGPU answer = CopyFrom(*this, this->_numDigits + right._numDigits);
  answer *= right;
  return answer;
}

BigFloatGPU& BigFloatGPU::operator*=(const BigFloatGPU& right) {
  uint32_t pSize = this->_numDigits + right._numDigits;
  if (pSize > this->_size * DIGITS_PER_BYTE) {
    std::stringstream ss;
    ss << "Product (" << pSize << ") would exceed the number of digits ("
       << this->_size * 2 << ") allocated for this BigFloatGPU";
    throw std::runtime_error(ss.str());
  }
  BigFloatGPU const *top;
  BigFloatGPU const *bottom;
  // top number has more digits    12345.6789
  //                               * 987654.3
  //                              ------------
  if (right._numDigits > this->_numDigits) {
    top = &right;
    bottom = this;
  }
  else {
    bottom = &right;
    top = this;
  }

  uint32_t tAfterDP = top->_numDigits - top->_dpPosition,
           bAfterDP = bottom->_numDigits - bottom->_dpPosition;

  uint32_t iWidth = bottom->_numDigits;
  uint32_t tDigits = top->_numDigits;

  //multiply top by each individual digit of bottom
  int threads = iWidth > THREADS_MAX ? THREADS_MAX : iWidth;
  int blocks = iWidth / threads;
//  memset(_intermediate, 0, intermediateSize);
  MultiplyIntermediate<<<blocks, threads>>>(top->_digits, tDigits,
    bottom->_digits, iWidth, _intermediate);
  cudaError_t cudaerr = cudaDeviceSynchronize();  //report kernel failure
  if (cudaerr != cudaSuccess) {
    std::stringstream ss;
    ss << "Kernel VectorMultiply failed with error \""
       << cudaGetErrorString(cudaerr) << "\".";
    throw std::runtime_error(ss.str());
  }

  uint32_t c = 0;
  char *product = (char *)calloc(pSize + 3, sizeof(char));
  int32_t pIdx = pSize + 1,
          dp = pSize - tAfterDP - bAfterDP + 1;  //sum # of decimal places

  threads = pSize;
  AddIntermediateProducts<<<blocks, threads>>>(pSize, tDigits, iWidth,
    _intermediate);
  cudaerr = cudaDeviceSynchronize();  //report kernel failure
  if (cudaerr != cudaSuccess) {
    std::stringstream ss;
    ss << "Kernel AddIntermediateProducts failed with error \""
       << cudaGetErrorString(cudaerr) << "\".";
    throw std::runtime_error(ss.str());
  }

#if PRINT_DEBUG
  printf("intermediate\n");
  for (uint32_t i = 0; i < pSize; ++i) {
    for (uint32_t j = 0; j < iWidth; ++j)
      printf("%-13d", _intermediate[i * iWidth + j]);
    printf("\n");
  }
#endif

  for (int32_t digit = pSize - 1; digit >= 0; --digit) {
    uint32_t sum = _intermediate[(pSize - 1 - digit) * iWidth];
    product[pIdx--] = ((sum + c) % BASE) + '0';
    if (pIdx == dp)
      product[pIdx--] = '.';
    c = (sum + c) / BASE;
  }

  while (pIdx >= 0)
    product[pIdx--] = '0';
  if (top->_sign != bottom->_sign)
    product[0] = '-';


  *this = BigFloatGPU(product, this->_size * DIGITS_PER_BYTE);
  free(product);
  return *this;
}
//#endregion

//#region Division
//Division
BigFloatGPU BigFloatGPU::operator/(const BigFloatGPU& right) {
  BigFloatGPU answer = CopyFrom(*this, this->_numDigits + right._numDigits);
  answer /= right;
  return answer;
}

BigFloatGPU& BigFloatGPU::operator/=(const BigFloatGPU& right) {

  BigFloatGPU const *top = this;
  BigFloatGPU const *bottom = &right;
  // no choice for top number        numerator
  //                               -------------
  //                                denominator

  uint32_t tSize = top->_size * DIGITS_PER_BYTE;
  uint32_t qSize = tSize + bottom->_numDigits - bottom->_dpPosition;

  char *quotient = (char *)calloc(qSize + 3, sizeof(char));
  int8_t *num = (int8_t *)calloc(qSize, sizeof(uint8_t));

  uint32_t numerDigits = top->_numDigits;
  DigitBlock blk;
  for (int i = 0; i < numerDigits; ++i) {
    if (i % BLOCK_DIGITS == 0)
      blk = top->_digits[i / BLOCK_DIGITS];
    num[i] = GET_DIGIT(blk, i);
  }


  BigFloatGPU den = CopyFrom(*bottom, tSize * 2);
  uint32_t bAfterDP = bottom->_numDigits - bottom->_dpPosition;
  den._dpPosition += bAfterDP;
  den._sign = true;
  std::stringstream ss;
  ss << den;
  den = ss.str();
  uint32_t winStart = 0,                      //start of window
           //shift quotient dp position
           dp = top->_dpPosition + 1 + bAfterDP,
           qIdx = 1;
  quotient[0] = '0';
  bool zero = false;
  blk = 0;

  //slide numerator comparison window until remainder zero or run out of
  //current space allocated for digits
  for (uint32_t shift = 0; shift < qSize && !zero; ++shift) {
    int32_t q = 0;
    BigFloatGPU window = DigitConcat(num + winStart, shift - winStart + 1);
    BigFloatGPU winRef = window;
    //how many times does denominator go into window
    for (; q <= BASE && !AbsLess(window, den, window._size * DIGITS_PER_BYTE);
         ++q)
      window -= den;
    if (q > BASE)
      printf("q > %d: %d\n", BASE, q);
    if (q) { //q goes into window at least once
      DigitBlock winBlk;

      //fill in zeros in numerator
      int32_t diff = winRef._dpPosition - window._dpPosition;
      for (int i = winStart; i < winStart + diff; ++i)
        num[i] = 0;

      //subtract from numerator
      winStart += diff;
      for (int i = winStart; i <= shift; ++i) {
        if ((i - winStart) % BLOCK_DIGITS == 0)
          winBlk = window._digits[(i - winStart) / BLOCK_DIGITS];
        num[i] = GET_DIGIT(winBlk, (i - winStart));
      }

      while (winStart < shift + 1 && num[winStart] == 0)
        ++winStart;
      quotient[qIdx++] = q + '0';  //add digit to answer
    }
    else
      quotient[qIdx++] = '0';
    if (qIdx == dp)
      quotient[qIdx++] = '.';

    //is the numerator (remainder) zero
    if (!zero) {
      zero = true;

      for (int i = winStart; i < qSize; ++i) {
        if (num[i] < 0)
          printf("_intermediate[i] < 0: %d !\n", _intermediate[i]);
        if (num[i] != 0) {
          zero = false;
          break;
        }
      }
    }
  }
  free(num);

  if (qIdx < qSize + 2)
    quotient[qIdx++] = '0';
  char *p = std::find(quotient, quotient + qSize + 3, '.');
  while (qIdx < qSize + 2 && p == quotient + qSize + 3) {
    if (qIdx == dp) {
      quotient[qIdx++] = '.';
      break;
    }
    else
      quotient[qIdx++] = '0';
  }
  if (right._sign != this->_sign)
    quotient[0] = '-';

  *this = BigFloatGPU(quotient, this->_size * DIGITS_PER_BYTE);
  free(quotient);
  return *this;
}
//#endregion

//#region Power
//Power
BigFloatGPU BigFloatGPU::operator^(const BigFloatGPU& right) {
  return BigFloatGPU(*this) ^= right;
}

BigFloatGPU& BigFloatGPU::operator^=(const BigFloatGPU& right) {
  return *this;
}
//#endregion

//#region Helpers
bool BigFloatGPU::AbsLess(const BigFloatGPU& newLeft, const BigFloatGPU& newRight,
                          uint32_t size) {
  uint32_t rAfterDP = newRight._numDigits - newRight._dpPosition,
           lAfterDP = newLeft._numDigits - newLeft._dpPosition,
           rBeforeDP = newRight._numDigits - rAfterDP,
           lBeforeDP = newLeft._numDigits - lAfterDP;
  if (lBeforeDP < rBeforeDP)
    return true;
  if (rBeforeDP < lBeforeDP)
    return false;

  DigitBlock lBlk, rBlk;
  uint32_t end = (rAfterDP > lAfterDP ? lAfterDP : rAfterDP) + lBeforeDP;
  for (uint32_t i = 0; i < end; ++i) {
    if (i % BLOCK_DIGITS == 0) {
      lBlk = newLeft._digits[i / BLOCK_DIGITS];
      rBlk = newRight._digits[i / BLOCK_DIGITS];
    }
    uint8_t l = GET_DIGIT(lBlk, i);
    uint8_t r = GET_DIGIT(rBlk, i);
    if (l < r)
      return true;
    if (r < l)
      return false;
  }

  return newLeft._numDigits < newRight._numDigits;
}

BigFloatGPU BigFloatGPU::DigitConcat(int8_t *start, int32_t length) {
  string str = "";
  for (int i = 0; i < length; ++i)
    str += *(start + i) + '0';

  return BigFloatGPU(str);
}

BigFloatGPU CopyFrom(const BigFloatGPU& right, uint32_t numDigits) {
  int64_t rSize = right._size * DIGITS_PER_BYTE;
  if (numDigits < rSize)
    numDigits = rSize;
  BigFloatGPU bf(numDigits);
  bf._numDigits = right._numDigits;
  bf._dpPosition = right._dpPosition;
  bf._postDpZeros = right._postDpZeros;
  bf._sign = right._sign;
  bf._layoutType = right._layoutType;
  bf._type = right._type;
  memcpy(bf._digits, right._digits, right._size);
  return bf;
}
//#endregion

//#region Copy and Output
BigFloatGPU& BigFloatGPU::operator=(const BigFloatGPU& right) {

  if (this->_digits != NULL)
    cudaFree(this->_digits);
  _numDigits = right._numDigits;
  _size = right._size;
  _dpPosition = right._dpPosition;
  _postDpZeros = right._postDpZeros;
  _sign = right._sign;
  _layoutType = right._layoutType;
  _type = right._type;
  cudaMallocManaged(&_digits, right._size);

  memcpy(this->_digits, right._digits, right._size);
  return *this;
}

//Assign string to BigFloatGPU
BigFloatGPU& BigFloatGPU::operator=(const string& newFloat) {

  if (!intermediateInitalized) {
    intermediateSize = sizeof(int) * _size * _size * DIGITS_PER_BYTE
                       * DIGITS_PER_BYTE * GLOBAL_INTERMEDIATE_FACTOR;
    cudaMallocManaged(&_intermediate, intermediateSize);
    intermediateInitalized = true;
#if PRINT_DEBUG
    printf("Intermediate Initialized\n");
#endif
  }
  string cFloat = FixExtraZeros(newFloat);
  _numDigits = DigitsOf(cFloat, &_dpPosition, &_sign, &_postDpZeros);
  if (_numDigits < 1) {    //string couldn't be interpreted as a float
    std::stringstream ss;
    ss << "BigFloatGPU string assignemnt: invalid float string: "
       << newFloat << "   " << cFloat << "\n";
    for (int i = 0; i < cFloat.size(); ++i)
      ss << (int)cFloat[i] << " ";
    ss << "\n";
    throw std::invalid_argument(ss.str().c_str());
  }

  //construction of integer bits from digits
  uint64_t d = 0;
  uint64_t blk = 0;
  uint32_t fChars = cFloat.size();
  for (uint32_t i = 0; i < fChars; ++i) {
    int64_t digit = cFloat[i] - '0';    //convert character to int
    if (digit >= 0 && digit < BASE) {
      blk |= SET_DIGIT(d, digit);
      if (d % BLOCK_DIGITS == BLOCK_DIGITS - 1) { //store 64-bit block
#if PRINT_DEBUG
        printf("%0*llX   ", BLOCK_DIGITS, blk);
#endif
        if (d < _size * DIGITS_PER_BYTE)
          _digits[d / BLOCK_DIGITS] = blk;
        blk = 0;
      }
      ++d;
    }
  }
#if PRINT_DEBUG
  printf("%0*llX   \n", BLOCK_DIGITS, blk);
#endif
  if (d < _size * DIGITS_PER_BYTE)
    _digits[d / BLOCK_DIGITS] = blk;

  return *this;
}

//Print out number as stream
std::ostream & operator<<(std::ostream &out, const BigFloatGPU &f) {
  uint32_t d = f._numDigits;
  if (!f._sign)
    out << '-';   //print negative sign if needed
  uint32_t dPos = f._dpPosition;
  uint64_t blk = 0;
  uint32_t i;

  for (i = 0; i < d; ++i) {
    if (i % BLOCK_DIGITS == 0)
      blk = f._digits[i / BLOCK_DIGITS]; //retreive 64-bit block
    if (i == dPos)  //output decimal point
      out << '.';
    out << GET_DIGIT(blk, i);   //retreive 4 bits from 64-bit block
  }
  if (i == dPos)
    out << '.';

  return out;
}
//#endregion
