/*
 BigFloatGPU2.h
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Library for CPU implementation of floating point arithmetic of greater than
   64-bit precision using 4 bits to store each digit
*/

#ifndef BIG_FLOAT_GPU_H
#define BIG_FLOAT_GPU_H

#include "BigFloatCommon.h"

class BigFloatGPU
{
private:
  uint32_t _numDigits;
  uint32_t _size;
  uint32_t _dpPosition;
  uint32_t _postDpZeros;
  bool _sign;
  BigFloatLayoutType _layoutType;
  BigFloatType _type;
  DigitBlock *_digits;
  static int *_intermediate;
  static bool intermediateInitalized;
  static int intermediateSize;
  //is the magnitude of left less than right
  bool AbsLess(const BigFloatGPU& left, const BigFloatGPU& right,
               uint32_t size);
public:
  BigFloatGPU();
  BigFloatGPU(const BigFloatGPU &newFloat);
  BigFloatGPU(uint32_t numDigits);
  //throws std::invalid_argument exception
  BigFloatGPU(double newFloat, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  //throws std::invalid_argument exception
  BigFloatGPU(const string &value, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  //throws std::invalid_argument exception
  BigFloatGPU(const char *value, uint32_t numDigits = BIG_FLOAT_DEF_DIGITS);
  ~BigFloatGPU();

  void Layout(BigFloatLayoutType layoutType);
  DigitBlock GetBlock(uint32_t idx) const;
  BigFloatLayoutType LayoutType() const;
  BigFloatType Type() const;
  uint32_t Size() const;
  uint32_t Digits() const;
  bool Sign() const;

  //Addition
  BigFloatGPU operator+(const BigFloatGPU& right);
  BigFloatGPU& operator+=(const BigFloatGPU& right);
  //Subtraction
  BigFloatGPU operator-(const BigFloatGPU& right);
  BigFloatGPU& operator-=(const BigFloatGPU& right);
  //Multiplication
  BigFloatGPU operator*(const BigFloatGPU& right);
  BigFloatGPU& operator*=(const BigFloatGPU& right);
  //Division
  BigFloatGPU operator/(const BigFloatGPU& right);
  BigFloatGPU& operator/=(const BigFloatGPU& right);
  //Power
  BigFloatGPU operator^(const BigFloatGPU& right);
  BigFloatGPU& operator^=(const BigFloatGPU& right);

  //Assembles a BigFloatGPU representing an integer with digits in array
  //from *start to start + length
  BigFloatGPU DigitConcat(int8_t *start, int32_t length);

  friend BigFloatGPU CopyFrom(const BigFloatGPU& right, uint32_t numDigits = 0);

  BigFloatGPU& operator=(const BigFloatGPU& right);
  //throws std::invalid_argument exception
  BigFloatGPU& operator=(const string& newFloat);
  //Print out number as stream
  friend std::ostream & operator<<(std::ostream &out, const BigFloatGPU &f);
};

#endif
