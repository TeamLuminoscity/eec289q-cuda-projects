/*
 Float128.cpp
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Implementation of Float128 class
*/

#include "MultiFloat.h"
#include "BigFloatCommon.h"

#define QUAD_EXPONENT(__VAL__) (QUAD_EXP_MASK | ((uint64_t)(__VAL__) <<\
        QUAD_EXP_SHIFT))
#define QUAD_UPPER_SIG(__VAL__) (((__VAL__) >> QUAD_DBL_SIG) &\
        QUAD_UPPER_SIG_MASK)
#define QUAD_LOWER_SIG(__VAL__) (DIGIT_UNMASK & ((__VAL__) <<\
        QUAD_SIG_SHIFT))

Float128 QUAD_ONE(0x3FFF000000000000, 0);
Float128 QUAD_TEN(0x4002400000000000, 0);
Float128 QUAD_INF(QUAD_EXP_MASK, 0);

Float128::Float128() {
  _sign = true;  //positive
  _layoutType = IEEE754;
  _type = MultipleCPU;
  _size = FLOAT_128_BITS / sizeof(DigitBlock) / BYTE;
}

Float128::Float128(const Float128 &f) {
  *this = f;
}

Float128::Float128(const double &d) {
  _sign = d < 0;
  uint64_t dBits;
  memcpy(&dBits, &d, sizeof(double));

  uint64_t upper = _sign ? 0 : SIGN_MASK;
  uint64_t lower = 0;
  uint16_t ex = (dBits & DBL_EXP_MASK) >> QUAD_EXP_SHIFT;
	if (ex == DBL_INF)
		if (_sign)
      *this = QUAD_INF;
    else {
      Float128 nInf(QUAD_EXP_MASK | SIGN_MASK, 0);
      *this = nInf;
    }
  else {
    lower = QUAD_LOWER_SIG(dBits);
    if (ex != 0)   //not a subnormal
      ex = ex - DBL_EXP_BIAS + QUAD_EXP_BIAS;
    upper |= QUAD_EXPONENT(ex) | QUAD_UPPER_SIG(dBits);
  }
  _digits[0] = upper;
  _digits[1] = lower;
}

Float128::Float128(const string &value) {
//  _sign = true;  //positive
  _layoutType = IEEE754;
  _type = MultipleCPU;
  _size = FLOAT_128_BITS / sizeof(DigitBlock) / BYTE;
  *this = value;
}

Float128::Float128(uint64_t upper, uint64_t lower) {
  _sign = (upper & SIGN_MASK) == 0;  //positive
  _layoutType = IEEE754;
  _type = MultipleCPU;
  _size = FLOAT_128_BITS / sizeof(DigitBlock) / BYTE;
  _digits[0] = upper;
  _digits[1] = lower;
}

//Addition
Float128& Float128::operator+(const Float128& right) {
  return Float128(*this) += right;
}

Float128& Float128::operator+=(const Float128& right) {

  return *this;
}

//Subtraction
Float128& Float128::operator-(const Float128& right) {
  return Float128(*this) -= right;
}

Float128& Float128::operator-=(const Float128& right) {
  return *this;
}

//Multiplication
Float128& Float128::operator*(const Float128& right) {
  return Float128(*this) *= right;
}

Float128& Float128::operator*=(const Float128& right) {
  return *this;
}

//Division
Float128& Float128::operator/(const Float128& right) {
  return Float128(*this) /= right;
}

Float128& Float128::operator/=(const Float128& right) {
  return *this;
}

//Power
Float128& Float128::operator^(const Float128& right) {
  return Float128(*this) ^= right;
}

Float128& Float128::operator^=(const Float128& right) {

  return *this;
}


//Assign string to float
Float128& Float128::operator=(const string& newFloat) {
  string cFloat = FixExtraZeros(newFloat);
  uint32_t digits = DigitsOf(cFloat, NULL, &_sign);

  if (digits < 1) {    //string couldn't be interpreted as a float
    std::stringstream ss;
    ss << "BigFloat string assignemnt: invalid float string: "
       << newFloat << "\n";
    throw std::invalid_argument(ss.str().c_str());
  }
  if (digits > MAX_128_DIGITS) {  //can only store 2 digits per byte
    std::stringstream ss;
    ss << "Float128 string assignment: Too many digits to store: "
       << digits << "\n" << "Float128 may store up to "
       << MAX_128_DIGITS << " digits\n";
    throw std::invalid_argument(ss.str().c_str());
  }

  Float128 result(0,0);
  int postDecimalDigits = 0;
  bool postDecimal = false;
  uint32_t end = cFloat.size();
  for (uint32_t i = _sign ? 0 : 1; i < end; ++i)
  {
    char digit = cFloat[i];
    if (digit >= '0' && digit <= '9')
    {
      Float128 fDigit((double)(digit - '0'));
      result *= QUAD_TEN;
      result += fDigit;
      if (postDecimal)
        postDecimalDigits++;
    }
    else if (digit == '.')
    {
      postDecimal = true;
    }
  /*  else if (digit == 'e' || digit == 'E')
    {
      postDecimalDigits -= atoi(cFloat.substr(i+1));
      break;
    } */
  }
  Float128 magnitude(QUAD_TEN ^ Float128((double) -postDecimalDigits));
  result *= magnitude;

  return *this;
}

Float128& Float128::operator=(const Float128 &f) {
  _sign = f._sign;  //positive
  _layoutType = f._layoutType;
  _type = f._type;
  _size = f._size;
  memcpy(_digits, f._digits, _size * sizeof(DigitBlock));
  return *this;
}

//print bits as sign|exponent and significand in Hex
string Float128::PrintBits() {
  std::stringstream ss;
  ss << std::hex << std::uppercase << std::setw(QUAD_EXP_HEX_DIGITS)
     << std::setfill('0') << (_digits[0] >> QUAD_EXP_SHIFT &
     QUAD_SIGN_EXP_MASK) << " " << std::setw(QUAD_EXP_SHIFT / BYTE)
     << (_digits[0] & QUAD_UPPER_SIG_MASK) << " " << std::setw(BLOCK_DIGITS)
     << _digits[1];
  return ss.str();
}

//Print out number as stream
std::ostream & operator<<(std::ostream &out, const Float128 &f) {
  return out;
}

//------------Inherited Functions-------------
uint32_t Float128::Size() const {
  return _size;
}

bool Float128::Sign() const {
  return _sign;
}
