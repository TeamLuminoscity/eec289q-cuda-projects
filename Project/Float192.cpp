/*
 Float192.cpp
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Implementation of Float192 class
*/

#include "MultiFloat.h"
#include "BigFloatCommon.h"

Float192::Float192() {
  _sign = true;  //positive
  _layoutType = IEEE754;
  _type = MultipleCPU;
  _size = FLOAT_192_BITS / sizeof(DigitBlock) / BYTE;
}

Float192::Float192(const Float192 &f) {
  *this = f;
}

Float192::Float192(const string &value) {
  _sign = true;  //positive
  _layoutType = IEEE754;
  _type = MultipleCPU;
  _size = FLOAT_192_BITS / sizeof(DigitBlock) / BYTE;
  *this = value;
}

//Addition
Float192& Float192::operator+(const Float192& right) {
  return *this;
}

//Subtraction
Float192& Float192::operator-(const Float192& right) {
  return *this;
}

//Multiplication
Float192& Float192::operator*(const Float192& right) {
  return *this;
}

//Division
Float192& Float192::operator/(const Float192& right) {
  return *this;
}


//---------------Compatibility Operators----------------
//Addition
Float192& Float192::operator+(const double& right) {
  return *this;
}

//Subtraction
Float192& Float192::operator-(const double& right) {
  return *this;
}

//Multiplication
Float192& Float192::operator*(const double& right) {
  return *this;
}

//Division
Float192& Float192::operator/(const double& right) {
  return *this;
}

//Assign string to BigFloat
Float192& Float192::operator=(const string& newFloat) {

  return *this;
}

Float192& Float192::operator=(const Float192 &f) {
  _sign = f._sign;  //positive
  _layoutType = f._layoutType;
  _type = f._type;
  _size = f._size;
  memcpy(_digits, f._digits, _size * sizeof(DigitBlock));
  return *this;
}

//Print out number as stream
std::ostream & operator<<(std::ostream &out, const Float192 &f) {
  return out;
}

//------------Inherited Functions-------------
uint32_t Float192::Size() const {
  return _size;
}

bool Float192::Sign() const {
  return _sign;
}
