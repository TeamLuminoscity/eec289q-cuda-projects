/*
 MultiFloat.h
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Library for CPU implementation of floating point arithmetic of greater than
   64-bit precision using a fixed format based on IEEE 254
*/

#ifndef MULTIFLOAT_H
#define MULTIFLOAT_H

#include "BigFloatCommon.h"

class MultiFloat
{
protected:
  uint32_t _size;
  bool _sign;  //positive
  BigFloatLayoutType _layoutType;
  BigFloatType _type;
public:
  virtual uint32_t Size() const = 0;
 // virtual uint32_t Digits() const = 0;
  virtual bool Sign() const = 0;
};

class Float128 : public MultiFloat
{
private:
  DigitBlock _digits[FLOAT_128_BITS / sizeof(DigitBlock) / BYTE];

public:
  Float128();
  Float128(const Float128 &f);
  Float128(const double &d);
  Float128(const string &value);
  Float128(uint64_t upper, uint64_t lower);
 // Float128(uint64_t integer, int E);

  //Addition
  Float128& operator+(const Float128& right);
  Float128& operator+=(const Float128& right);
  //Subtraction
  Float128& operator-(const Float128& right);
  Float128& operator-=(const Float128& right);
  //Multiplication
  Float128& operator*(const Float128& right);
  Float128& operator*=(const Float128& right);
  //Division
  Float128& operator/(const Float128& right);
  Float128& operator/=(const Float128& right);
  //Power
  Float128& operator^(const Float128& right);
  Float128& operator^=(const Float128& right);
  //Assign string to float
  Float128& operator=(const string& newFloat);
  //Copy
  Float128& operator=(const Float128 &f);
  //print bits as sign|exponent and significand in Hex
  string PrintBits();
  //Print out number as stream
  friend std::ostream & operator<<(std::ostream &out, const Float128 &f);

  //------------Inherited Functions-------------
  uint32_t Size() const;
 // uint32_t Digits() const;
  bool Sign() const;
};

class Float192 : public MultiFloat
{
private:
  DigitBlock _digits[FLOAT_192_BITS / sizeof(DigitBlock) / BYTE];
public:
  Float192();
  Float192(const Float192 &f);
  Float192(const string &value);

  //Addition
  Float192& operator+(const Float192& right);
  //Subtraction
  Float192& operator-(const Float192& right);
  //Multiplication
  Float192& operator*(const Float192& right);
  //Division
  Float192& operator/(const Float192& right);

  //----------Compatibility Operators-----------
  //Addition
  Float192& operator+(const double& right);
  //Subtraction
  Float192& operator-(const double& right);
  //Multiplication
  Float192& operator*(const double& right);
  //Division
  Float192& operator/(const double& right);
  //Assign string to float
  Float192& operator=(const string& right);
  Float192& operator=(const Float192 &f);
  //Print out number as stream
  friend std::ostream & operator<<(std::ostream &out, const Float192 &f);

  //------------Inherited Functions-------------
  uint32_t Size() const;
 // uint32_t Digits() const;
  bool Sign() const;
};

#endif
