/*
 testLibraryGPU.cu
 Tim Ambrose and Robbie Sadre
 27 February 2018
   Tests the BigFloat and BigFloatGPU libraries
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string>
#include <stdexcept>
#include "BigFloat.h"
#include "BigFloatGPU.h"

using std::string;
using std::cout;


int main(int argc, char const *argv[]) {
  try {
    BigFloatGPU bf_1 = "12345.678901";
    BigFloatGPU bf_2 = "9876543210.9";
    BigFloatGPU bf_3 = ".12345678901";
    BigFloatGPU bf_4 = "-9.8765432109";
    BigFloatGPU bf_5 = ".98765432109";
    BigFloatGPU bf_6 = "12345678901";
    BigFloatGPU bf_7 = "98765432109.";
    BigFloatGPU bf_8 = "-.12345678901";
    BigFloatGPU bf_9 = "-98765432109.";
    BigFloatGPU bf_10 = "-5.0";
    BigFloatGPU bf_11 = "0.0000000001";
    BigFloatGPU bf_12 = 99.;
    BigFloatGPU bf_13 = 12300000000.;
    BigFloatGPU bf_14 = "0.0000202499999979015937502174473476337170186014560739474241143371981761518078389";
    BigFloatGPU bf_15 = "-2.0";
    BigFloatGPU bf_16 = "-0.0001620000014610780131774626008475351970439199421391139581526687885789198041932";
    BigFloatGPU bf_17 = "98765.432109";
  //  BigFloatGPU bf_18("4960794035231711966409854833835079882936317343136598236382598928159508453779019868790791814805708040702712372.72555281431855499143345920024160650921213335474561697751297498703562741992463204893174161247449998719729433106392920827598593116819300148409572063014786459111172041073576232951347547557083637921330771820869333100865002625494658542002560515578577733737563030644766301974966137107144913885499987525133329663401130262047825601414375942982936361701997505", 464);
  //  BigFloatGPU bf_19("94035231711966409854833835079882936317343136598236382598928159508453779019868790791814805708040702712372.72555281431855499143345920024160650921213335474561697751297498703562741992463204893174161247449998719729433106392920827598593116819300148409572063014786459111172041073576232951347547557083637921330771820869333100865002625494658542002560515578577733737563030644766301974966137107144913885499987525133329663401130262047825601414375942982936361701997505", 464);


   /* BigFloatGPU bf_1("12345.678901", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_2("9876543210.9", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_3(".12345678901", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_4("-9.8765432109", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_5(".98765432109", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_6("12345678901", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_7("98765432109.", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_8("-.12345678901", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_9("-98765432109.", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_10(-5.0, BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_11("0.00000000001", BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_12(99., BIG_FLOAT_DEF_DIGITS * 2);
    BigFloatGPU bf_13(12300000000., BIG_FLOAT_DEF_DIGITS * 2); */
  //  BigFloatGPU bf_14("0.0000202499999979015937502174473476337170186014560739474241143371981761518078389", 25000);
  //  BigFloatGPU bf_15 = "-2.0";
  //  BigFloatGPU bf_16("-0.0001620000014610780131774626008475351970439199421391139581526687885789198041932", 25000);

   /* cout << "Result:\n" << bf_1 << "\n" << bf_2 << "\n" << bf_3 << "\n";
    cout << bf_4 << "\n" << bf_5 << "\n" << bf_6 << "\n";
    cout << bf_7 << "\n" << bf_8 << "\n" << bf_9 << "\n";
    cout << bf_10 << "\ntimes:\n"; */
    cout << "multiply:\n";
    cout << bf_1 * bf_16 << "\n";
    cout << bf_17 * bf_14 << "\n";
    cout << bf_1 * bf_1 << "\n";
    cout << bf_1 * bf_11 << "\n";
    cout << bf_2 * bf_12 << "\n";
    cout << bf_2 * bf_13 << "\nadd:\n";
    cout << bf_1 + bf_2 << "\n";
    cout << bf_4 + bf_8 << "\n";
    cout << bf_1 + bf_10 << "\n";
    cout << bf_4 + bf_5 << "\nsub:\n";
    cout << bf_2 - bf_1 << "\n";
    cout << bf_4 - bf_8 << "\n";
    cout << bf_1 - bf_2 << "\n";
    cout << bf_6 - bf_9 << "\ndiv:\n";
    cout << bf_15 / bf_11 << "\n";
    cout << bf_11 / bf_15 << "\n";
    cout << bf_1 / bf_15 << "\n";
    cout << bf_15 / bf_1 << "\n";
    cout << bf_10 / bf_15 << "\n";
    cout << bf_15 / bf_10 << "\n";
    cout << bf_15 / bf_14 << "\n";
    cout << bf_15 / bf_16 << "\n";
  }
  catch (std::invalid_argument &crush) {
    std::cout << "Exception caught:\n" << crush.what() << "\n";
  }
  catch (std::runtime_error &crush) {
    std::cout << "Exception caught:\n" << crush.what() << "\n";
  }
  //exit(0);
  return 0;
}
